
import React from 'react';
import UserRoutes from './Screen/UserRoutes';

import { View } from 'native-base';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { AsyncStorage } from 'react-native';
import Config from './Screen/Config';
c = new Config


export default class App extends React.Component {
  
  constructor(props) {
    super(props);
    global.PrimeryColor ='#3d6bb4'
  }


  
  state = {
        isLoadingComplete: false,
        isLogin: "false",
      };
      componentDidMount() {
            console.disableYellowBox = true;
            this.getKey();
          }
        
          _loadResourcesAsync = async () => Promise.all([
            Font.loadAsync({
              // 'Kan': require('./assets/fonts/Nurkholis.ttf'),
              'Kan': require('./assets/fonts/GE-Flow-Regular-1.otf'),
              // 
            }),
            
          ]);
        
          _handleLoadingError = (error) => {
            console.warn(error);
          };
        
          _handleFinishLoading = () => {
            this.setState({ isLoadingComplete: true });
          };
        
          async getKey() {
            try {
              
            
              const L_isLogin = await AsyncStorage.getItem('@MySuperStore:isLogin');
             let PrimeryColor1 = await AsyncStorage.getItem('@MySuperStore:PrimeryColor');
             if (PrimeryColor1 === null) {
              PrimeryColor1=  '#f39c12' ;
            }
            c.getTranslations1(PrimeryColor1);
              this.setState({ isLogin: L_isLogin });
             global.PrimeryColor = PrimeryColor1;
            } catch (error) {
              console.log("Error retrieving data" + error);
            }
          }
        

          
  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    }

    
      return <UserRoutes />;
   

  }}