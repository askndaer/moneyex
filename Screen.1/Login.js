import React from 'react';
import { StyleSheet, Text, Alert, View, ImageBackground, AsyncStorage, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { NavigationActions } from 'react-navigation';
import colors from './Color';
const logo = require("../assets/logo.png");

export default class login extends React.Component {
 
  constructor(props) {
    super(props);

    this.navigationOptions = {
      drawerLockMode: 'locked-open',
      };

    this.state = {
      UserEmail: 'fares',
      UserPassword: '123'
    };
   
  }

  async saveKeyEmail_Name_phone(Email, Name, Phone) {
    try {
      await AsyncStorage.setItem('@MySuperStore:Email', Email);
      await AsyncStorage.setItem('@MySuperStore:Name', Name);
      await AsyncStorage.setItem('@MySuperStore:Phone', Phone);
      await AsyncStorage.setItem('@MySuperStore:Type', 'true');
    } catch (error) {
      console.log("Error saving data" + error);
    }
  }


  async saveLogin() {
    try {
      await AsyncStorage.setItem('@MySuperStore:isLogin', "ture");
      console.log("11111111111111111111111111111111111111111");
    } catch (error) {
    }
  }

 
  GuestLogin = async  ()  => {
    await AsyncStorage.setItem('@MySuperStore:Type', "false");
    console.log("11111111111111111111111111111111111111111");
    this.props.navigation.navigate('Main');
 };

  
  // async GuestLogin() {
  //   try {
  //  //   await AsyncStorage.setItem('@MySuperStore:Type', "false");
  //     console.log("11111111111111111111111111111111111111111");
  //     this.navigateToScreen('Main')
    
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }
  navigateToScreen(route) {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
      header: null,
    });
    this.props.navigation.navigate('LoginN',navigateAction);
  }



  UserLoginFunction = () => {


    const { UserEmail } = this.state;
    const { UserPassword } = this.state;


    fetch('http://172.16.132.50/7/User_Login.php', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({

        email: UserEmail,

        password: UserPassword

      })

    })
    .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        var pieces = responseJson.split("<>");
        // If server response message same as Data Matched
        if (pieces[0] === 'Data Matched') {

          this.saveLogin();
          this.saveKeyEmail_Name_phone(pieces[1], pieces[2], pieces[3]);
           this.navigateToScreen('Main');

        }
        else {

          Alert.alert(responseJson);
        }

      }).catch(
        function(error) {
          alert("Please Check internet connection");
          response="";
        }
      )

  }


  render() {

    return (

      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/b1.png')}>

        <Animatable.View animation="slideInDown" iterationCount={1} direction="alternate" style={styles.TopCon}>
          <Image source={logo} style={styles.logo} resizeMode="contain" />
        </Animatable.View>

        <Animatable.View animation="fadeInUp" iterationCount={1} direction="alternate" style={styles.ButtomCon}>
          <TouchableOpacity style={styles.buttonContainer} onPress={this.GuestLogin}>
            <Text style={styles.textButton} > دخول كـ ضيف</Text>
          </TouchableOpacity>


          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <View style={{ flexDirection: 'row', width: '70%', marginTop: 15, marginBottom: 15 }}>
              <View style={{ backgroundColor: colors.PrimeryColor, height: 2, flex: 1, alignSelf: 'center' }} />
              <Text style={{ color: colors.PrimeryColor, fontSize: 20, alignSelf: 'center', paddingHorizontal: 5 }}>   أو   </Text>
              <View style={{ backgroundColor: colors.PrimeryColor, height: 2, flex: 1, alignSelf: 'center' }} />
            </View>
          </View>

          <TextInput
            style={styles.inputText}
            placeholder="عنوان البريد"
            placeholderTextColor="#724f14"
            onChangeText={UserEmail => this.setState({ UserEmail })}
          />
          <TextInput
            style={styles.inputText}
            placeholder="كلمة السر"
            placeholderTextColor="#724f14"
            onChangeText={UserPassword => this.setState({ UserPassword })}
          />

          <TouchableOpacity style={styles.buttonContainer} onPress={this.UserLoginFunction}>
            <Text style={styles.textButton} > دخول</Text>
          </TouchableOpacity>


          <TouchableOpacity onPress={() => this.props.navigation.navigate('user_register')}>
            <Text style={styles.TButton} > انشاء مستخدم  جديد</Text>
          </TouchableOpacity>
        </Animatable.View>


      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {

    flex: 1,

    alignItems: 'center',
    justifyContent: 'center',
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  TopCon: {
    flex: 1,

  },
  ButtomCon: {
    flexDirection: 'column',
    flex: 2,
    justifyContent: 'flex-end'

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 20,
    height: 40,

  }, textButton: {
    color: '#333133',
    fontSize: 15,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
  inputText: {
    fontFamily: 'Kan',
    color: colors.PrimeryColor,
    fontSize: 15,
    width: '70%',
    backgroundColor: '#ffffff0C',
    flexDirection: 'row',
    paddingVertical: 5,
    borderRadius: 3,
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    width: '70%',
    marginTop: 10,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    textAlign: "center",
    alignSelf: "center",

  },
  TButton: {
    fontFamily: 'Kan',
    marginTop: 30,
    height: 40,
    textAlign: "center",
    alignSelf: "center",
    color: colors.PrimeryColor,
  },

  logo: {
    width: '60%',
    marginTop: 150,
    alignSelf: "center",
  }
});
