import React from 'react';
import { StyleSheet,SegmentedControlIOS, Text, View, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container,  Content, Textarea,Form, Item, Input, Label, Right } from 'native-base';
import colors from './Color';
const logo = require("../assets/logo.png");
import SwitchSelector from 'react-native-switch-selector';
import { Divider } from 'react-native-elements';
import { Drawer,Thumbnail } from 'native-base';
import Header from './Header';
import MySiderBar from './MySiderBar';
import SideBar from './MySiderBar';



export default class user_register extends React.Component {


  render() {

    closeDrawer = () => {
      this.drawer._root.close()
    };
    openDrawer = () => {
      //  alert('fsd');
      this.drawer._root.open()
    };

    return (



      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/main.png')}>
        
<Header  navigation={this.props.navigation}  />
  

           <Text style={styles.TheH1}>
           تغيير كلمة السر
            </Text>
        
        
     <View style={{marginLeft:'20%',marginRight:'20%'}}>
      
    
        </View>
        <Container style={{ backgroundColor: '#ffffff00', margin: 20 , flex:8,}}>
          <Content > 
          <Animatable.View ref="view1"  animation="slideInUp" iterationCount={1} direction="alternate" >
            <Form style={styles.Myfrom}>
              <Item stackedLabel >
                <Label style={styles.Label1}>كلمة المرورالحالية </Label>
                <Input style={styles.MyInput} />
              </Item>
              <Divider style={{ backgroundColor: '#ffffff00',margin:20, }} />
                 <Item stackedLabel last>
                <Label style={styles.Label1}> كلمة الجديدة  </Label>
                <Input style={styles.MyInput} />
              </Item>
                 <Item stackedLabel last>
                <Label style={styles.Label1}> تاكيد كلمة السر الجديدة  </Label>
                <Input style={styles.MyInput} />
              </Item>

             
            </Form>
            </Animatable.View>
          </Content>
    


        </Container>

<View  style={{flex:4}}>
<TouchableOpacity style={styles.buttonContainer} onPress={()=> this._openDrawer1()}>
            <Text style={styles.textButton} > تسجيل </Text>
          </TouchableOpacity>


</View>

      </ImageBackground>
     
    );
  }
}

const styles = StyleSheet.create({

  imgBackground: {
    width: '100%',
    height: '100%',

  },
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 30,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 25,
    flex:1,
  },
  Label1: {

    color: colors.SecondColor,
    textAlign: "right",
    fontFamily: 'Kan',
    width: '100%',
    marginTop: 20,
  },
  MyInput: {
    height: 30,
    textAlign: 'right',
    width: '100%',
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    color: colors.PrimeryColor,
  },
  Myfrom: {
    borderRadius: 20,
    padding: 20,

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 20,
    height: 40,

  }
  , textButton: {
    color: '#333133',
    fontSize: 15,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
});
