import React from 'react';
import { StyleSheet,SegmentedControlIOS, Text, View, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container,  Content, Textarea,Form, Item, Input, Label, Right } from 'native-base';
import colors from './Color';
const logo = require("../assets/logo.png");
import SwitchSelector from 'react-native-switch-selector';
import { CheckBox } from 'react-native-elements';
import { Drawer,Picker } from 'native-base';
import Header from './Header';
import MySiderBar from './MySiderBar';
import SideBar from './MySiderBar';



export default class user_register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "key1"
    };
  }
  onValueChange(value) {
    this.setState({
      selected: value
    });
  }

  render() {

    closeDrawer = () => {
      this.drawer._root.close()
    };
    openDrawer = () => {
      //  alert('fsd');
      this.drawer._root.open()
    };

    return (

      <Drawer  
      ref={(ref) => { this.drawer = ref; }}
      content={<SideBar navigator={this.navigator} />}
      closeDrawer={() => this.closeDrawer()} 
      side="right"
      
      type="displace" 
      panOpenMask={.10}
      // onOpen={_openDrawer1} 
      >
<Header />

      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/main.png')}>


  

           <Text style={styles.TheH1}>
انشاء شكوى جديدة
            </Text>
        
        
     <View style={{marginLeft:'20%',marginRight:'20%'}}>
      
    
        </View>
        <Container style={{ backgroundColor: '#ffffff00', margin: 20 , flex:8,}}>
          <Content > 
          <Animatable.View ref="view1"  animation="slideInUp" iterationCount={1} direction="alternate" >
            <Form style={styles.Myfrom}>
              <Item stackedLabel >
                <Label style={styles.Label1}>الوكالة  </Label>
                <Input style={[styles.MyInput,{textAlign:"center"}]} value="العالمية للصرافة "/>
              </Item>

              
           

             
                 <Item stackedLabel last>
                <Label style={styles.Label1}>نوع المشكلة  </Label>
         
                <Picker
              note
              mode="dropdown"
              style={{ width: '100%' }}
              placeholder='نوع المشكلة '
              selectedValue={this.state.selected}
              onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label="Wallet" value="key0" />
              <Picker.Item label="ATM Card" value="key1" />
              <Picker.Item label="Debit Card" value="key2" />
              <Picker.Item label="Credit Card" value="key3" />
              <Picker.Item label="Net Banking" value="key4" />
            </Picker>
              </Item>
               

  <Item stackedLabel last>
                <Label style={styles.Label1}>تفاصيل المشكلة  </Label>
         
                <Textarea rowSpan={3} bordered placeholder="اكتب هنا عنوان تفاصيل المشكلة" style={[styles.MyInput,{height:60,}]} />
              </Item>
             

   <CheckBox
          title="اتعهد بأن كل ماذكر صحيك واتحمل التبعات القانونيه ان وحدت اتعهد بأن كل ماذكر صحيك واتحمل التبعات القانونيه ان وحدت اتعهد بأن كل ماذكر صحيك واتحمل التبعات القانونيه ان وحدت"
          right='true'
          iconRight='true'
          containerStyle={{backgroundColor:'#ffffff00',flex:1}}
          checked={this.state.checked3}
          onPress={() => this.setState({ checked3: !this.state.checked3 })}
        />


            </Form>
            </Animatable.View>
          </Content>
    


        </Container>

<View  style={{flex:4}}>
<TouchableOpacity style={styles.buttonContainer} onPress={()=> this._openDrawer1()}>
            <Text style={styles.textButton} > تسجيل </Text>
          </TouchableOpacity>


</View>

      </ImageBackground>
      </Drawer>
    );
  }
}

const styles = StyleSheet.create({

  imgBackground: {
    width: '100%',
    height: '100%',

  },
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 30,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 25,
    flex:1,
  },
  Label1: {

    color: colors.SecondColor,
    textAlign: "right",
    fontFamily: 'Kan',
    width: '100%',
    marginTop: 20,
  },
  MyInput: {
    height: 30,
    textAlign: 'right',
    width: '100%',
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    color: colors.PrimeryColor,
  },
  Myfrom: {
    borderRadius: 20,
    padding: 20,

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 20,
    height: 40,

  }
  , textButton: {
    color: '#333133',
    fontSize: 15,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
});
