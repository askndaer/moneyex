import React from 'react';
import { StyleSheet, Text, View, ImageBackground,AsyncStorage,TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Drawer } from 'native-base';
import SideBar from './MySiderBar';
import colors from './Color';
const menu = require("../assets/icons/menu.png");
import { withNavigation } from 'react-navigation';


export default class Header extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            isType: 'false',
       
    
        };
        
    }
    
      componentWillMount() {
     
        this.getKey()
     }
     
    async getKey() {
        try {
          const L_isType= await AsyncStorage.getItem('@MySuperStore:Type');
            this.setState({isType: L_isType});
            console.log(L_isType);
        } catch (error) {
          console.log("Error retrieving data" + error);
        }
      }
    

  render() {
    return (


    <View style={{height:60,width:'100%',backgroundColor:colors.SecondColor,justifyContent:"center",flexDirection:"row"}}>
<Text style={styles.HeaderTEXT} >Application Name</Text>

<View  style={styles.HeaderIcon} >
{this.state.isType=='true' ?
<TouchableOpacity style={styles.Boxs}  onPress={() => { this.props.navigation.openDrawer() }}>
<Image source={menu} style={styles.MenuItemImg} resizeMode="contain" />
</TouchableOpacity>
:
<TouchableOpacity style={styles.Boxs} ></TouchableOpacity>

}
</View>




            
    </View>
    );
}
}

const styles = StyleSheet.create({
 
  
    MenuItemImg:{
  marginTop:20,
height:27,
width:27,


textAlign: "center",
alignSelf: "center",
    },
    HeaderTEXT:{
        height:'100%',

        justifyContent:"center",
        textAlign:"right",
        flex:9,
        textAlignVertical: "bottom",
        color:colors.PrimeryColor,
        fontSize: 20,
        fontFamily: 'Kan',
            },

        HeaderIcon:{
        flex:1,
        height:'100%',

        justifyContent:"center",
        textAlign:"right",
        // textAlignVertical: "bottom",
    },


  });
  