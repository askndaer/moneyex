export default {
  container: {
    paddingTop: 20,
    flex: 1
  },

  navSectionStyle: {
    backgroundColor: '#fff'
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5
  },
  footerContainer: {
    padding: 20,
    backgroundColor: '#3c4673',
    alignItems:'center' ,
  }
};
