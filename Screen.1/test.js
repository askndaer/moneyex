// import React from 'react';
// import { StyleSheet, Text, View,Button, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
// import * as Animatable from 'react-native-animatable';
// import { Container, Header, Content, Form, Item, Input, Label, Right } from 'native-base';
// import colors from './Color';
// const logo = require("../assets/logo.png");



// export default class user_register extends React.Component {

//     renderNext(){
//         this.refs.view1.fadeInLeft(300);
//         // alert("asdfas");
// }
 
// _openDrawer1 = () => {
//     this.refs.view1.fadeInLeft(300);
// };
// render() {
// return (
//     <View style={{margin:150}}>
// <Animatable.View ref="view1">
// <Text >fasdfasd</Text>
// </Animatable.View>
// <TouchableOpacity onPress={()=> this._openDrawer1()}>
// <Text >fsadf</Text>

// </TouchableOpacity>
 
 
// </View>

// );
// }
// }

import React from 'react';
import {
  Notifications,
} from 'expo';
import {
  Text,
  View,
} from 'react-native';

// This refers to the function defined earlier in this guide
import registerForPushNotificationsAsync from './registerForPushNotificationsAsync';

export default class AppContainer extends React.Component {
  state = {
    notification: {},
  };

  componentDidMount() {
    registerForPushNotificationsAsync();

    // Handle notifications that are received or selected while the app
    // is open. If the app was closed and then opened by tapping the
    // notification (rather than just tapping the app icon to open it),
    // this function will fire on the next tick after the app starts
    // with the notification data.
    this._notificationSubscription = Notifications.addListener(this._handleNotification);
  }

  _handleNotification = (notification) => {
    this.setState({notification: notification});
  };

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Origin: {this.state.notification.origin}</Text>
        <Text>Data: {JSON.stringify(this.state.notification.data)}</Text>
      </View>
    );
  }
}