import React from 'react';
import { StyleSheet,SegmentedControlIOS, Text, View, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container, Header, Content, Textarea,Form, Item, Input, Label, Right } from 'native-base';
import colors from './Color';
const logo = require("../assets/logo.png");
import SwitchSelector from 'react-native-switch-selector';
import { CheckBox } from 'react-native-elements';
const options = [
   { label: 'وكالة', value: '1' },
  { label: 'مستخدم', value: '2' },
 
];


export default class user_register extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      gender:true,
      checked1: false,
      checked2: false,
      checked3: false,
      checked4: false,
      };
    
}



_openDrawer1 = (v) => {

  if(v=='1'){
    
    this.setState({ gender: false })
    // this.refs.view1.fadeOutLeft(300);
    
    }else{
    this.setState({ gender: true })
    // this.refs.view1.fadeInLeft(300);
    // alert('1111');
    }
};

  render() {


    renderNext = (v) => {
      if(v=='1'){
      this.setState({ gender: false })
      // alert('dfasd');
      }else{
      this.setState({ gender: true })
      // alert('1111');
      }
    };
    return (

      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/main.png')}>


  

           <Text style={styles.TheH1}>
            انشاء حساب حديد
            </Text>
        
        
     <View style={{marginLeft:'20%',marginRight:'20%'}}>
      
     <SwitchSelector
    initial={1}
    onPress={value =>  this._openDrawer1(value)}
    textColor={colors.PrimeryColor} //'#7a44cf'
    selectedColor={colors.SecondColor}
    buttonColor={colors.PrimeryColor}
    borderColor={colors.SecondColor}
    backgroundColor={colors.SecondColor}
    hasPadding
    options={options} 
    bold='true'
    />
        </View>
        <Container style={{ backgroundColor: '#ffffff00', margin: 20 , flex:9,}}>
         

    {this.state.gender ?


          <Content > 
          <Animatable.View ref="view1">
            <Form style={styles.Myfrom}>
              <Item stackedLabel >
                <Label style={styles.Label1}>اسم المستخدم</Label>
                <Input style={styles.MyInput} />
              </Item>

                 <Item stackedLabel last>
                <Label style={styles.Label1}> البريد الالكتروني </Label>
                <Input style={styles.MyInput} />
              </Item>


              <Item stackedLabel last>
                <Label style={styles.Label1}>كلمة السر</Label>
                <Input style={styles.MyInput} />
              </Item>

              <Item stackedLabel last>
                <Label style={styles.Label1}>رقم الهاتف </Label>
                <Input style={styles.MyInput} />
              </Item>

           
              <Item stackedLabel last>
                <Label style={styles.Label1}> العنوان</Label>
                <Input style={styles.MyInput} />
              </Item>
            </Form>
            </Animatable.View>
          </Content>
    
: 

<Content > 
<Animatable.View ref="view1">
  <Form style={styles.Myfrom}>
    <Item stackedLabel >
      <Label style={styles.Label1}>اسم الوكالة</Label>
      <Input style={styles.MyInput} />
    </Item>

     <Item stackedLabel >
      <Label style={styles.Label1}>اسم المدير</Label>
      <Input style={styles.MyInput} />
    </Item>

       <Item stackedLabel last>
      <Label style={styles.Label1}> البريد الالكتروني </Label>
      <Input style={styles.MyInput} />
    </Item>


    <Item stackedLabel last>
      <Label style={styles.Label1}>كلمة السر</Label>
      <Input style={styles.MyInput} />
    </Item>

    <Item stackedLabel last>
      <Label style={styles.Label1}>رقم الهاتف </Label>
      <Input style={styles.MyInput} />
    </Item>
    <Item stackedLabel last>
      <Label style={styles.Label1}>رقم الوتس اب </Label>
      <Input style={styles.MyInput} />
    </Item>
    <Item stackedLabel last>
      <Label style={styles.Label1}>رقم الفيبر </Label>
      <Input style={styles.MyInput} />
    </Item>
 
    <Item stackedLabel last>
      <Label style={styles.Label1}> العنوان</Label>
      <Input style={styles.MyInput} />
    </Item>

     <Item stackedLabel last>
      <Label style={styles.Label1}>رقم الفيبر </Label>
      <Input style={styles.MyInput} />
    </Item>

      <Item stackedLabel last>
      <Label style={styles.Label1}> العملات المتدولة </Label>
    <View style={{flexDirection:'row'}}>
   
         <CheckBox
          title="اليورو"
          right='true'
          iconRight='true'
          containerStyle={{backgroundColor:'#ffffff00',flex:1}}
          checked={this.state.checked2}
          onPress={() => this.setState({ checked2: !this.state.checked2 })}
        />
         <CheckBox
          title="الدولار"
          right='true'
          iconRight='true'
       
          containerStyle={{backgroundColor:'#ffffff00',flex:1}}
          checked={this.state.checked1}
          onPress={() => this.setState({ checked1: !this.state.checked1 })}
        />
</View>
</Item>
<Item stackedLabel last>
      <Label style={styles.Label1}>  متواجد في  </Label>
    <View style={{flexDirection:'row'}}>
   
         <CheckBox
          title="مصر"
          right='true'
          iconRight='true'
          containerStyle={{backgroundColor:'#ffffff00',flex:1}}
          checked={this.state.checked3}
          onPress={() => this.setState({ checked3: !this.state.checked3 })}
        />
         <CheckBox
          title="ليبيا"
          right='true'
          iconRight='true'
       
          containerStyle={{backgroundColor:'#ffffff00',flex:1}}
          checked={this.state.checked4}
          onPress={() => this.setState({ checked4: !this.state.checked4 })}
        />
</View>
</Item>


  <Item stackedLabel last>
                <Label style={styles.Label1}>عنوان الشركة  </Label>
         
                <Textarea rowSpan={3} bordered placeholder="اكتب هنا عنوان الشركة بكامل" style={[styles.MyInput,{height:60,}]} />
              </Item>


  </Form>
  </Animatable.View>
</Content>


}

        </Container>

<View  style={{flex:2}}>
<TouchableOpacity style={styles.buttonContainer} onPress={()=> this._openDrawer1()}>
            <Text style={styles.textButton} > تسجيل </Text>
          </TouchableOpacity>


</View>

      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({

  imgBackground: {
    width: '100%',
    height: '100%',

  },
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 30,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 35,
    flex:1,
  },
  Label1: {

    color: colors.SecondColor,
    textAlign: "right",
    fontFamily: 'Kan',
    width: '100%',
    marginTop: 20,
  },
  MyInput: {
    height: 30,
    textAlign: 'right',
    width: '100%',
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    color: colors.PrimeryColor,
  },
  Myfrom: {
    borderRadius: 20,
    padding: 20,

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 20,
    height: 40,

  }
  , textButton: {
    color: '#333133',
    fontSize: 15,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
});
