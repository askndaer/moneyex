import React from 'react';
import { StyleSheet, Text, View, ImageBackground, AsyncStorage, TouchableOpacity, Image } from 'react-native';
import colors from './Color';
import { Drawer, Thumbnail } from 'native-base';
const profile = require("../assets/icons/profile.png");
const password = require("../assets/icons/password.png");
const contact = require("../assets/icons/contact.png");
const about = require("../assets/icons/about.png");
const logout = require("../assets/icons/logout.png");

import * as Animatable from 'react-native-animatable';

import { NavigationActions } from 'react-navigation';



const uri = "http://profileplugin.com/wp-content/uploads/upme/1444024111_harvey-2.jpg"

export default class MySiderBar extends React.Component {



    constructor() {
        super();

        this.state = {
            Email: 'No Email',
            Name: 'No Name',
            Phone: 'No Phone',

        }
        this.getKey();
    }


    navigateToScreen = (route) => () => {

        const navigateAction = NavigationActions.navigate({
            routeName: route,
            header: null,
        });
        this.props.navigation.dispatch(navigateAction);
    }

    // closeDrawer = () => {
    //     this.drawer._root.close()
    //   };

    logout1 = async  ()  => {
        try {
                await AsyncStorage.setItem('@MySuperStore:isLogin', "false");
    
            } catch (error) {
                console.log("Error saving data" + error);
            }
        this.props.navigation.navigate('LogOutN');
       };


         
        
        // 
       
       // 






    async getKey() {
        try {
            //  this.props.navigation.navigate('Pricelist');
            //             console.log("Error retrieving data222222222222222222");
            const L_Email = await AsyncStorage.getItem('@MySuperStore:Email');
            const L_Name = await AsyncStorage.getItem('@MySuperStore:Name');
            const L_Phone = await AsyncStorage.getItem('@MySuperStore:Phone');

            this.setState({ Email: L_Email });
            this.setState({ Name: L_Name });
            this.setState({ Phone: L_Phone });

        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }
    render() {

        return (
            <ImageBackground style={styles.imgBackground}
                resizeMode='cover'
                source={require('../assets/slider.png')}>


                <View style={styles.MenuHeader}>

                    <Thumbnail circle='true' style={{ flex: 3, width: 150, height: 150,  marginTop: 60, }} large source={{ uri: uri }} />
                    <Text style={[styles.MenuItemTEXT, { flex: 1 }]}  >{this.state.Name}</Text>
                </View>




                <Animatable.View style={[styles.MenuBody]} ref="view">


                    <TouchableOpacity style={styles.MenuItem} onPress={this.navigateToScreen('Main')}>
                        <Text style={styles.MenuItemTEXT} >الملف الشخصي</Text>
                        <Image source={profile} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>


                    <TouchableOpacity style={styles.MenuItem} onPress={this.navigateToScreen('passwordChange')}>
                        <Text style={styles.MenuItemTEXT} >تحديث كلمة المرور</Text>
                        <Image source={password} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.MenuItem} onPress={this.navigateToScreen('passwordChange')}>
                        <Text style={styles.MenuItemTEXT} >الشكاوي  </Text>
                        <Image source={password} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.MenuItem} onPress={this.navigateToScreen('passwordChange')}>
                        <Text style={styles.MenuItemTEXT} >مراسلة الادارة</Text>
                        <Image source={contact} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.MenuItem} onPress={this.navigateToScreen('passwordChange')}>
                        <Text style={styles.MenuItemTEXT} >عن التطبيق</Text>
                        <Image source={about} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>
                </Animatable.View>
                <View style={styles.MenuFooter}>
                    <TouchableOpacity style={styles.MenuItem}  onPress={this.logout1} >
                        <Text style={styles.MenuItemTEXT} >تسجيل الخروج</Text>
                        <Image source={logout} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>
                </View>

            </ImageBackground>



        );
    }
}


const styles = StyleSheet.create({

    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1,
    },

    MenuItem: {
        color: colors.PrimeryColor,
        fontSize: 15,
        width: '100%',
        backgroundColor: '#ffffff0C',
        flexDirection: 'row',
        paddingVertical: 5,
        borderRadius: 3,
        borderColor: colors.PrimeryColor,
        borderWidth: 1,
        marginTop: 10,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        textAlign: "center",
        alignSelf: "center",
    },

    MenuItemImg: {
        height: 30,
        width: 30,
        margin: 5,
        flex: 1,
        justifyContent: 'flex-end',

    },

    MenuItemTEXT: {
        height: 40,
        justifyContent: 'center',
        flex: 5,
    ///////    textAlignVertical: "center",
        color: colors.PrimeryColor,
        fontSize: 20,
        fontFamily: 'Kan',
    },

    MenuHeader: {
        flex: 3,
        justifyContent: 'center', alignSelf: 'center'
    },

    MenuBody: {
        flex: 8,
    },

    MenuFooter: {
        flex: 1,
    }

});
