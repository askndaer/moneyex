import React from 'react';
import { StyleSheet,BackHandler, Text, View,Alert, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Drawer,Thumbnail } from 'native-base';
import MySiderBar from './MySiderBar';
import colors from './Color';
import Header from './Header';
import { DrawerNavigator } from "react-navigation";


export default class Main extends React.Component {

  constructor (props) {
    super(props);
    
    this.state = {
      BuyUSD: '',
      SellUSD: '',
      BuyUo:'',
      SellUo:''

    };
    
}

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    this.GetDataFromServer();
 }
 
 componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
 }


 GetDataFromServer1 = () =>{
  console.log("3333333333333333333");
  closeDrawer();
  console.log("222222222")
  Main.props.navigation.navigate('Pricelist');
  console.log("11111111111");
 }
 GetDataFromServer = () =>{
  
  
  fetch('http://172.16.132.50/7/getrange.php', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
        
  }).then((response) => response.json())
        .then((responseJson) => {
   
         var pieces = responseJson.split("<>");
     

          this.setState({BuyUSD: pieces[0]});
          this.setState({SellUSD: pieces[1]});
          this.setState({BuyUo: pieces[2]});
          this.setState({SellUo: pieces[3]});
        }).catch((error) => {
          console.error(error);
        });
   
   
    }
  



  backPressed = () => {
    Alert.alert(
      'Exit App',
      'Do you want to exit?',
      [
        {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Yes', onPress: () => BackHandler.exitApp()},
      ],
      { cancelable: false });
      return true;
  }
  
  render() {
   
      closeDrawer = () => {
        this.drawer._root.close()
      };
      openDrawer = () => {
        this.drawer._root.open()
      };



const greeting = 'Welcome to React';
      return (
      
 

  
      <ImageBackground style={styles.imgBackground} ref="view1"
        resizeMode='cover'
        source={require('../assets/main.png')}>

<Header  navigation={this.props.navigation}  />

        <Animatable.View animation="slideInDown" iterationCount={1} direction="alternate" style={styles.TopCon}>
       
          <Text style={styles.TheH1} >
            أسعار العملات
       </Text>

        </Animatable.View>

        <View style={styles.ButtomCon}>

          <View style={styles.TheMainBox}>
            <Text style={styles.MainTitle} > سعر الدولار </Text>
            <Animatable.View animation="slideInDown" iterationCount={1} direction="alternate" >
              <View style={styles.MainBox}>
                <TouchableOpacity style={styles.Boxs} onPress={openDrawer}>
                  <Text style={styles.TextTitle} > متوسط الشراء </Text>
                  <Text style={styles.TextNumber} >{this.state.BuyUSD}</Text>
                </TouchableOpacity>


                <TouchableOpacity style={styles.Boxs} >
                  <Text style={styles.TextTitle}  >متوسط البيع </Text>
                  <Text style={styles.TextNumber} >{this.state.SellUSD}</Text>
                </TouchableOpacity>

              </View>

            </Animatable.View>
          </View>


          <View style={styles.TheMainBox}>
            <Text style={styles.MainTitle} > سعر اليورو </Text>
            <Animatable.View animation="slideInDown" iterationCount={1} direction="alternate" >
              <View style={styles.MainBox}>
                <TouchableOpacity style={styles.Boxs} onPress={this.userRegister}>
                  <Text style={styles.TextTitle} > متوسط الشراء </Text>
                  <Text style={styles.TextNumber} >{this.state.BuyUo}</Text>
                </TouchableOpacity>


                <TouchableOpacity style={styles.Boxs} >
                  <Text style={styles.TextTitle}  > متوسط البيع </Text>
                  <Text style={styles.TextNumber} >{this.state.SellUo}</Text>
                </TouchableOpacity>

              </View>

              

            </Animatable.View>
            
          </View>

        </View>

        <View style={styles.Footer}>

            <View style={{flexDirection:"row",justifyContent:'space-around'}}>
            <TouchableOpacity style={[styles.SmallbuttonContainer,{backgroundColor:'#27ae60'}]} onPress={this.NextScreen}>
            <Text style={styles.textButton} > بيع عملة</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.SmallbuttonContainer,{backgroundColor:'#2980b9'}]} onPress={this.NextScreen}>
            <Text style={styles.textButton} > شراء عملة</Text>
            </TouchableOpacity>

            
        </View>
            <TouchableOpacity style={styles.buttonContainer} onPress={this.GetDataFromServer}>
            <Text style={styles.textButton} >تحديث الاسعار</Text>
            </TouchableOpacity>


        </View>

      </ImageBackground>
  
    );
  }
}

const styles = StyleSheet.create({
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 30,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 25,

  },
  TheMainBox: {
    backgroundColor: colors.SecondColor,
    margin:20,
    borderRadius: 20,

  },
  MainBox: {
    flexDirection: 'row',
    width: '100%',
    //  height:'20%',
    justifyContent: "center",

  },

  Boxs: {
    width: '40%',
    // height: '10%',
    justifyContent: "center",
    alignSelf: "center",
    
  },
  MainTitle: {
    backgroundColor:'#fff',
    color:colors.SecondColor,
    fontSize: 20,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    marginBottom:5,
    padding:10,
    marginTop:-30,
    borderRadius: 20,
    borderColor:colors.SecondColor,
  
    borderWidth:2,
  },
  TextTitle: {
    color: colors.PrimeryColor,
    fontSize: 20,

    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    marginTop:20,
  },


  TextNumber: {
    color: colors.SecondColor,
    fontSize: 28,
    width:60,
    height:60,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin:10,
    marginBottom:30,
    backgroundColor:colors.PrimeryColor,
    borderRadius: 60/2,
    borderColor:'#fff',
    paddingTop:5,
    borderWidth:2,
  },

  imgBackground: {
    width: '100%',
    height: '100%',
flex:1,
  },
  TopCon: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "center",
  },
  ButtomCon: {
    flex: 5,
    justifyContent: "center",
  },
  Footer: {
    flex: 2,
    justifyContent:'flex-end',
    marginBottom:20,
  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '90%',
    borderRadius: 6,
    marginTop: 10,
    height: 40,

  }, textButton: {
    color: '#333133',
    fontSize: 15,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
  },

  SmallbuttonContainer:{
    // backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '40%',
    borderRadius: 6,
    marginBottom: 10,

    height: 40,
  },
  inputText: {
    color: colors.PrimeryColor,
    fontSize: 15,
    width: '70%',
    backgroundColor: '#ffffff0C',
    flexDirection: 'row',
    paddingVertical: 5,
    borderRadius: 3,
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    width: '70%',
    marginTop: 10,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    textAlign: "center",
    alignSelf: "center",

  },
  TButton: {
    marginTop: 30,
    height: 40,
    textAlign: "center",
    alignSelf: "center",
    color: colors.PrimeryColor,
  },
  logo: {
    width: '60%',
    marginTop: 150,
    alignSelf: "center",

  }
});
