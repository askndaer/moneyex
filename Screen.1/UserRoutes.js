import React from 'react';
import { AsyncStorage } from 'react-native';

import MainPageScreen from './user_register';
import MainPageScreen1 from './user_register1';
import SideMenu from './MySiderBar';
import LoginScreen from './Login';
import MainScreen from './Main';
import user_registerScreen from './user_register';
import TestScreen from './test';
import PricelistScreen from './pricelist';
import passwordChangeScreen from './passwordChange';
import NewCompScreen from './NewComp';
import comListScreen from './comList';
import ViewCompScreen from './ViewComp';



import { DrawerNavigator, createSwitchNavigator, createDrawerNavigator } from 'react-navigation';
import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation';

const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 750,
      // easing: Easing.out(Easing.poly(4)),
      easing: Easing.back(1),
      // easing: Easing.
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps

      const thisSceneIndex = scene.index
      const width = layout.initWidth

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0],
      })

      return { transform: [{ translateX }] }
    },
  }
}



class AuthLoadingScreen extends React.Component {
  
  _bootstrapAsync = async () => {
    console.log('123');
    const userToken = await AsyncStorage.getItem('userToken');
    // const appFirstTimeToken = await AsyncStorage.getItem('appFirstTime');

    if (appFirstTimeToken) {
      this.props.navigation.navigate(userToken ? 'LogOutN' : 'LoginN');
    } else {
      this.props.navigation.navigate('AppFirstTime');
    }

  };

  constructor() {
    super();
    console.log('345');
    // this.props.navigation.navigate(userToke)
    this._bootstrapAsync();
  }


}


const LoginNavigator = createDrawerNavigator({

  // Login: { screen: LoginScreen, navigationOptions: {   drawerLockMode: 'locked-open',header: 'das',}, },
  Main: { screen: MainScreen, navigationOptions: { header: null } },
  // user_register: { screen:user_registerScreen, navigationOptions: {header: null}, },
  passwordChange: { screen: passwordChangeScreen, navigationOptions: { header: null }, },
}, {
    contentComponent: SideMenu,
    drawerPosition: 'right',
    drawerWidth: 300,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',

    drawerType: 'back',
  },

);



const LogOutNavigator = createDrawerNavigator({

  Login: { screen: LoginScreen, navigationOptions: { drawerLockMode: 'locked-open', header: 'das', }, },
  // Main: { screen: MainScreen, navigationOptions: {header: null} },
  user_register: { screen: user_registerScreen, navigationOptions: { header: null }, },
  // passwordChange: { screen:passwordChangeScreen, navigationOptions: {header: null}, },
},
  {
    drawerLockMode: 'locked-closed'
  }, {
    contentComponent: SideMenu,
    drawerPosition: 'right',
    drawerWidth: 300,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',

    drawerType: 'back',
  },

);

const AppNavigator = createSwitchNavigator(

  {
    
    AuthLoading: {screen: AuthLoadingScreen},
    LogOutN: {screen: LogOutNavigator},
    LoginN: {screen: LoginNavigator},
  },
  {
    initialRouteName: 'AuthLoading',
  }
);




const App = createAppContainer(AppNavigator);
export default App



