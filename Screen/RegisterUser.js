import React from 'react';
import { StyleSheet, AsyncStorage, SegmentedControlIOS, KeyboardAvoidingView, Alert, Text, View, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container, Header, Content, Textarea, Form, Item, Input, Label, Right } from 'native-base';
import colors from './Color';
import { NavigationActions } from 'react-navigation';
const logo = require("../assets/logo.png");
import SwitchSelector from 'react-native-switch-selector';
import Header1 from './Header';
import { CheckBox } from 'react-native-elements';
import HideWithKeyboard from 'react-native-hide-with-keyboard';
import { Spinner } from 'native-base';
import ModalDropdown from 'react-native-modal-dropdown';


export default class user_register extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      client_name: '',
      phone_number: '',
      email: '',
      password: '',
      address: '',

      UserType: true,
      checked1: false,
      checked2: false,
      checked3: false,
      checked4: false,
      countryCode: null


    };


  }
  _openDrawer1 = (v) => {

    if (v == '1') {
      this.refs.view1.fadeInUp(1200);
      this.setState({ UserType: false })
      // this.refs.view1.fadeOutLeft(300);
      this.clean();
    } else {
      this.setState({ UserType: true })
      // this.refs.view1.fadeInLeft(300);
      // alert('1111');
      this.refs.view1.fadeInUp(1200);
      this.clean();
    }
  };


  async saveKeyEmail_Name_phone(Email, Name, Phone) {
    try {
      await AsyncStorage.setItem('@MySuperStore:Email', Email);
      await AsyncStorage.setItem('@MySuperStore:Name', Name);
      await AsyncStorage.setItem('@MySuperStore:Phone', Phone);
      await AsyncStorage.setItem('@MySuperStore:Type', 'true');
    } catch (error) {
      console.log("Error saving data" + error);
    }
  }


  async saveLogin() {
    try {
      await AsyncStorage.setItem('userToken', "ture");
      console.log("11111111111111111111111111111111111111111");
    } catch (error) {
    }
  }

  navigateToScreen(route) {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
      header: null,
    });
    this.props.navigation.navigate('LoginN', navigateAction);
  }


  clean = () => {
    this.setState({ client_name: '' });
    this.setState({ phone_number: '' });
    this.setState({ email: '' });
    this.setState({ password: '' });
    this.setState({ address: '' });


  }
  validate = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return true;
    }
    else {
      return false;
    }
  }

  phonenumber(inputtxt) {

    if (inputtxt.length < 7)

      return true;

    else


      return false;

  }

  UserLoginFunction = () => {

    const { client_name } = this.state;
    const { phone_number } = this.state;
    const { email } = this.state;
    const { password } = this.state;
    const { address } = this.state;
    if (client_name == '' || phone_number == '' || email == '' || password == '' || address == '') {
      Alert.alert('تنبية', 'يرجاء ادخال البيانات');
      return;
    }


    if (this.phonenumber(phone_number)) {
      Alert.alert('تنبية', 'يرجى ادخل الهاتف بشكل صحيح');
      return
    }

    const { countryCode } = this.state;

    let x = 0;
    if (countryCode == 0)
      x = "00218"
    if (countryCode == 1)
      x = "0020"
    if (countryCode == 2)
      x = "0090"

    let Mybody = "client_name=" + client_name + "&" +
      "phone_number=" + x + phone_number + "&" +
      "email=" + email + "&" +
      "password=" + password + "&" +
      "address=" + address + "";
    this.setState({
      SpinnerS: true
    })
    console.log("11111111111111111111111111111111111111111");
    console.log(Mybody);
    fetch('http://konuze.com/index.php/api/example/addclient', {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
      }),
      body: Mybody
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          SpinnerS: false
        })
        console.log("11111111111111111111111111111111111111111");
        console.log(responseJson);
        let MyResponse = responseJson[0];
        if (MyResponse.response == 1) {
          Alert.alert("تنبية", MyResponse.message);
          this.props.navigation.navigate('Login');

        } else {
          Alert.alert("تنبية", MyResponse.message);
        }
      }).catch((error) => {
        Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
        this.setState({
          SpinnerS: false
        })
      });



  }
  render() {


    return (

      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/main.png')}>
        <Header1 navigation={this.props.navigation} />
        <HideWithKeyboard>


          <Text style={styles.TheH1}>
            انشاء حساب حديد
            </Text>


        </HideWithKeyboard>

        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding"  >
          <Container style={{ backgroundColor: '#ffffff00', margin: 20, }}>


            {this.state.UserType ?

              <Content >
                <Animatable.View ref="view1">
                  <Form style={styles.Myfrom}>
                    <Item stackedLabel >
                      <Label style={styles.Label1}>اسم المستخدم</Label>
                      <Input style={styles.MyInput} value={this.state.client_name} value={this.state.client_name} onChangeText={client_name => this.setState({ client_name })} />

                    </Item>

                    <Item stackedLabel last>
                      <Label style={styles.Label1}> البريد الالكتروني </Label>
                      <Input style={styles.MyInput} value={this.state.email} onChangeText={email => this.setState({ email })} />
                    </Item>


                    <Item stackedLabel last>
                      <Label style={styles.Label1}>كلمة السر</Label>
                      <Input style={styles.MyInput} value={this.state.password} onChangeText={password => this.setState({ password })} />
                    </Item>
                    <Item stackedLabel >
                      <Label style={styles.Label1}>الدولة</Label>
                      <ModalDropdown
                        defaultValue={'يرجى اختيار الدولة'}
                        textStyle={{ textAlign: 'right', fontSize: 14, height: 45, justifyContent: 'center', color: "#737272", lineHeight: 45 }}
                        dropdownStyle={{
                          height: 180,
                          textAlign: 'right',
                          width: '88%',
                          marginTop: -25,
                          marginLeft: -23
                        }}
                        onSelect={(v) => { this.setState({ countryCode: v }) }}
                        dropdownTextStyle={{ lineHeight: 45, fontSize: 14, textAlign: 'right', marginRight: 20 }}
                        style={[styles.MyInput, { justifyContent: 'center' }]}
                        options={['ليبيا', 'مصر', 'تركيا']} />
                    </Item>
                    <Item stackedLabel last>
                      <Label style={styles.Label1}>رقم الهاتف </Label>
                      <Input style={styles.MyInput} value={this.state.phone_number} onChangeText={phone_number => this.setState({ phone_number })} />
                      <Text style={{ position: 'absolute', bottom: 12, left: 20 }}>
                        {this.state.countryCode == 0 && '00218'}
                        {this.state.countryCode == 1 && '0020'}
                        {this.state.countryCode == 2 && '0090'}
                      </Text>
                    </Item>


                    <Item stackedLabel last>
                      <Label style={styles.Label1}> العنوان</Label>
                      <Input style={styles.MyInput} value={this.state.address} onChangeText={address => this.setState({ address })} />
                    </Item>
                  </Form>
                </Animatable.View>
              </Content>

              :

              <Content >
                <Animatable.View ref="view1">
                  <Form style={styles.Myfrom}>
                    <Item stackedLabel >
                      <Label style={styles.Label1}>اسم الشركة</Label>
                      <Input value={this.state.company_name1} onChangeText={company_name1 => this.setState({ company_name1 })} style={styles.MyInput} />
                    </Item>

                    <Item stackedLabel >
                      <Label style={styles.Label1}>اسم المدير</Label>
                      <Input value={this.state.manager_name1} onChangeText={manager_name1 => this.setState({ manager_name1 })} style={styles.MyInput} />
                    </Item>

                    <Item stackedLabel last>
                      <Label style={styles.Label1}> البريد الالكتروني </Label>
                      <Input value={this.state.email1} onChangeText={email1 => this.setState({ email1 })} style={styles.MyInput} />
                    </Item>


                    <Item stackedLabel last>
                      <Label style={styles.Label1}>كلمة السر</Label>
                      <Input value={this.state.password1} onChangeText={password1 => this.setState({ password1 })} style={styles.MyInput} />
                    </Item>

                    <Item stackedLabel last>
                      <Label style={styles.Label1}>رقم الهاتف </Label>
                      <Input value={this.state.Phone1} onChangeText={Phone1 => this.setState({ Phone1 })} style={styles.MyInput} />
                    </Item>
                    <Item stackedLabel last>
                      <Label style={styles.Label1}>رقم الوتس اب </Label>
                      <Input value={this.state.whatsup_number1} onChangeText={whatsup_number1 => this.setState({ whatsup_number1 })} style={styles.MyInput} />
                    </Item>
                    <Item stackedLabel last>
                      <Label style={styles.Label1}>رقم الفيبر </Label>
                      <Input value={this.state.viber_number1} onChangeText={viber_number1 => this.setState({ viber_number1 })} style={styles.MyInput} />
                    </Item>

                    <Item stackedLabel last>
                      <Label style={styles.Label1}> العملات المتدولة </Label>
                      <View style={{ flexDirection: 'row' }}>

                        <CheckBox
                          title="اليورو"
                          right='true'
                          iconRight='true'
                          containerStyle={{ backgroundColor: '#ffffff00', borderColor: '#ffffff00' }}
                          checked={this.state.checked2}
                          onPress={() => this.setState({ checked2: !this.state.checked2 })}
                        />
                        <CheckBox
                          title="الدولار"
                          right='true'
                          iconRight='true'

                          containerStyle={{ backgroundColor: '#ffffff00', borderColor: '#ffffff00' }}
                          checked={this.state.checked1}
                          onPress={() => this.setState({ checked1: !this.state.checked1 })}
                        />
                      </View>
                    </Item>
                    <Item stackedLabel last>
                      <Label style={styles.Label1}>  متواجد في  </Label>
                      <View style={{ flexDirection: 'row' }}>

                        <CheckBox
                          title="مصر"
                          right='true'
                          iconRight='true'
                          containerStyle={{ backgroundColor: '#ffffff00', borderColor: '#ffffff00' }}
                          checked={this.state.place11}
                          onPress={() => this.setState({ place11: !this.state.place11 })}
                        />
                        <CheckBox
                          title="ليبيا"
                          right='true'
                          iconRight='true'

                          containerStyle={{ backgroundColor: '#ffffff00', borderColor: '#ffffff00' }}
                          checked={this.state.place21}
                          onPress={() => this.setState({ place21: !this.state.place21 })}
                        />
                      </View>
                    </Item>


                    <Item stackedLabel last>
                      <Label style={styles.Label1}>عنوان الشركة  </Label>

                      <Textarea value={this.state.address1} onChangeText={address1 => this.setState({ address1 })} rowSpan={3} bordered placeholder="اكتب هنا عنوان الشركة بكامل" style={[styles.MyInput, { height: 60, }]} />
                    </Item>


                  </Form>
                </Animatable.View>
              </Content>


            }

          </Container>

          <View style={{}}>
            <TouchableOpacity style={[styles.buttonContainer, { backgroundColor: c.PrimeryColor }]} onPress={() => this.UserLoginFunction()}>

              {this.state.SpinnerS ?
                <Spinner color='red' />
                :
                <Text style={styles.textButton} > تسجيل </Text>
              }

            </TouchableOpacity>


          </View>
        </KeyboardAvoidingView>
      </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({

  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1
  },
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 18,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 10,


  },
  Label1: {

    color: colors.PrimeryColor,
    textAlign: "right",
    fontFamily: 'Kan',
    width: '100%',
    marginTop: 20,
  },
  MyInput: {
    height: 30,
    textAlign: 'right',
    width: '100%',
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    color: colors.PrimeryColor,
  },
  Myfrom: {
    borderRadius: 20,
    padding: 20,

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 10,
    height: 40,
    marginBottom: 20

  }
  , textButton: {
    color: colors.SecondColor,
    fontSize: 12,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
});
