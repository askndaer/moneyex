import React from 'react';
import { StyleSheet,AsyncStorage,SegmentedControlIOS,KeyboardAvoidingView,Alert, Text, View, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container, Header, Content, Textarea,Form, Item, Input, Label, Right } from 'native-base';
import colors from './Color';
import { NavigationActions } from 'react-navigation';
const logo = require("../assets/logo.png");
import SwitchSelector from 'react-native-switch-selector';
import Header1 from './Header';
import { CheckBox } from 'react-native-elements';
import HideWithKeyboard from 'react-native-hide-with-keyboard';
import {  Spinner } from 'native-base';
import Link from './conf'
import ModalDropdown from 'react-native-modal-dropdown';

export default class user_register extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      

      UserType:true,
      checked1: false,
      checked2: false,
      checked3: false,
      checked4: false,

      company_name1: '',
      manager_name1: '',
      viber_number1: '',
      whatsup_number1:'',
      email1:'',
      Phone1:'',
      
      address1:'',
      place11: false,
      place21: false,
      place21: false,
      password1:'',

      countryCode:1

     
      };
  
}
_openDrawer1 = (v) => {

  if(v=='1'){
    this.refs.view1.fadeInUp(1200);
    this.setState({ UserType: false })
    // this.refs.view1.fadeOutLeft(300);
    this.clean() ;
    }else{
    this.setState({ UserType: true })
    // this.refs.view1.fadeInLeft(300);
    // alert('1111');
    this.refs.view1.fadeInUp(1200);
    this.clean() ;
    }
};


async saveKeyEmail_Name_phone(Email, Name, Phone) {
  try {
    await AsyncStorage.setItem('@MySuperStore:Email', Email);
    await AsyncStorage.setItem('@MySuperStore:Name', Name);
    await AsyncStorage.setItem('@MySuperStore:Phone', Phone);
    await AsyncStorage.setItem('@MySuperStore:Type', 'true');
  } catch (error) {
    console.log("Error saving data" + error);
  }
}


async saveLogin() {
  try {
    await AsyncStorage.setItem('userToken', "ture");
    console.log("11111111111111111111111111111111111111111");
  } catch (error) {
  }
}

navigateToScreen(route) {
  const navigateAction = NavigationActions.navigate({
    routeName: route,
    header: null,
  });
  this.props.navigation.navigate('LoginN',navigateAction);
}



validate = (text) => {
  console.log(text);
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
  if(reg.test(text) === false)
  {
  return true;
    }
  else {
    return false;
  }
  }

  UserLoginFunction = () => {
 
    const { company_name1 } = this.state;
    const { manager_name1 } = this.state;
    const { viber_number1 } = this.state;
    const {whatsup_number1 } = this.state;

   
    const { Phone1 } = this.state;
    const { email1 } = this.state;
    const { address1 } = this.state;
    const { place11 } = this.state;
    const { place21 } = this.state;
    const { place31 } = this.state;
    const { password1 } = this.state;
    const { countryCode } = this.state;

    let x=0;
    if(countryCode==0)
      x="00218"
      if(countryCode==1)
      x="0020"
      if(countryCode==2)
      x="0090"

      let w=0;
      if(countryCode==0)
        w="+218"
        if(countryCode==1)
        w="+20"
        if(countryCode==2)
        w="+90"
let MyForm = new FormData();
MyForm.append("company_name", company_name1)
MyForm.append("manager_name", manager_name1)
MyForm.append("viber_number", w+viber_number1)
MyForm.append("email", email1)
MyForm.append("phone_number", x+Phone1)
MyForm.append("whatsup_number", w+whatsup_number1)
MyForm.append("address", address1)
MyForm.append("place1", place11)
MyForm.append("place2", place21)
MyForm.append("password", password1)
MyForm.append("place3", place31)

    this.setState({
      SpinnerS: true
    })
    console.log("11111111111111111111111111111111111111111");
  
    fetch("http://konuze.com/index.php/api/example/"+'add_company', {
      method: 'POST',
        headers: new Headers({
          'Content-Type': 'multipart/form-data',
       
        }),
      body: MyForm
       })
    
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        SpinnerS: false
      })
      console.log("111111111111111111111111111111111111111113333333333333");
      console.log(responseJson);
      let MyResponse = responseJson[0];
      if (MyResponse.response==1){
        Alert.alert("تنبية", MyResponse.message);
        this.props.navigation.navigate('Login');
       
      }else{
        Alert.alert("تنبية", MyResponse.message);
      }
     }
     )
     .catch((error) => {
       Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
    console.log(error);
      this.setState({
        SpinnerS: false
      })
    });
   
  }


  render() {


    return (

      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/main.png')}>
<Header1  navigation={this.props.navigation}  />
<HideWithKeyboard>


           <Text style={styles.TheH1}>
            انشاء حساب شركة جديدة
            </Text>
        
       
        </HideWithKeyboard>

      <KeyboardAvoidingView style={{flex:1}} behavior="padding"  > 
        <Container style={{ backgroundColor: '#ffffff00', margin: 20 , }}>
         

    

<Content > 
<Animatable.View ref="view1">
  <Form style={styles.Myfrom}>
    <Item stackedLabel >
      <Label style={styles.Label1}>اسم الشركة</Label>
      <Input  value={this.state.company_name1} onChangeText={company_name1 => this.setState({ company_name1 })} style={styles.MyInput} />
    </Item>

     <Item stackedLabel >
      <Label style={styles.Label1}>اسم المدير</Label>
      <Input value={this.state.manager_name1} onChangeText={manager_name1 => this.setState({ manager_name1 })} style={styles.MyInput} />
    </Item>

       <Item stackedLabel >
      <Label style={styles.Label1}> البريد الالكتروني </Label>
      <Input  value={this.state.email1} onChangeText={email1 => this.setState({ email1 })} style={styles.MyInput} />
    </Item>


    <Item stackedLabel >
      <Label style={styles.Label1}>كلمة السر</Label>
      <Input value={this.state.password1} onChangeText={password1 => this.setState({ password1 })} style={styles.MyInput} />
    </Item>
    <Item stackedLabel >
      <Label style={styles.Label1}>الدولة</Label>
    <ModalDropdown 
    defaultValue={'يرجى اختيار الدولة'}
    textStyle={{ textAlign: 'right',fontSize:14,height:45,justifyContent:'center',color:"#737272",lineHeight:45}}
    dropdownStyle={{height: 180,
    textAlign: 'right',
    width: '88%',
    marginTop:-25,
    marginLeft:-23
   }}
   onSelect={(v)=>{this.setState({countryCode:v})}}
   dropdownTextStyle={{lineHeight:45,fontSize:14,textAlign: 'right',marginRight:20}}
    style={[styles.MyInput,{justifyContent:'center'}]} 
    options={['ليبيا', 'مصر','تركيا']}/>
  </Item>

    <Item stackedLabel >
      <Label style={styles.Label1}>رقم الهاتف </Label>
      <Input value={this.state.Phone1} onChangeText={Phone1 => this.setState({ Phone1 })} style={styles.MyInput} />
      <Text style={{position:'absolute',bottom:12,left:20}}>
      {this.state.countryCode==0&&'00218'}
      {this.state.countryCode==1&&'0020'}
      {this.state.countryCode==2&&'0090'}
      </Text>
    </Item>




    <Item stackedLabel >
      <Label style={styles.Label1}>رقم الوتس اب </Label>
      <Input value={this.state.whatsup_number1} onChangeText={whatsup_number1 => this.setState({ whatsup_number1 })} style={styles.MyInput} />
      <Text style={{position:'absolute',bottom:12,left:20}}>
      {this.state.countryCode==0&&'00218'}
      {this.state.countryCode==1&&'0020'}
      {this.state.countryCode==2&&'0090'}
      </Text>
    </Item>
    <Item stackedLabel >
      <Label style={styles.Label1}>رقم الفيبر </Label>
      <Input value={this.state.viber_number1} onChangeText={viber_number1 => this.setState({ viber_number1 })} style={styles.MyInput} />
      <Text style={{position:'absolute',bottom:12,left:20}}>
      {this.state.countryCode==0&&'00218'}
      {this.state.countryCode==1&&'0020'}
      {this.state.countryCode==2&&'0090'}
      </Text>
    </Item>
 
      <Item stackedLabel >
      <Label style={styles.Label1}> العملات المتدولة </Label>
    <View style={{flexDirection:'row'}}>
   
         <CheckBox
          title="اليورو"
          right='true'
          iconRight='true'
          containerStyle={{backgroundColor:'#ffffff00',borderColor:'#ffffff00'}}
          checked={this.state.checked2}
          onPress={() => this.setState({ checked2: !this.state.checked2 })}
        />
         <CheckBox
          title="الدولار"
          right='true'
          iconRight='true'
       
          containerStyle={{backgroundColor:'#ffffff00',borderColor:'#ffffff00'}}
          checked={this.state.checked1}
          onPress={() => this.setState({ checked1: !this.state.checked1 })}
        />
</View>
</Item>
<Item stackedLabel >
      <Label style={styles.Label1}>  متواجد في  </Label>
    <View style={{flexDirection:'row'}}>
   
         <CheckBox
          title="مصر"
          right='true'
          iconRight='true'
          containerStyle={{backgroundColor:'#ffffff00',borderColor:'#ffffff00'}}
          checked={this.state.place11}
          onPress={() => this.setState({ place11: !this.state.place11 })}
        />
         <CheckBox
          title="ليبيا"
          right='true'
          iconRight='true'
       
          containerStyle={{backgroundColor:'#ffffff00',borderColor:'#ffffff00'}}
          checked={this.state.place21}
          onPress={() => this.setState({ place21: !this.state.place21 })}
        />
</View>
</Item>


  <Item stackedLabel >
                <Label style={styles.Label1}>عنوان الشركة  </Label>
         
                <Textarea value={this.state.address1} onChangeText={address1 => this.setState({ address1 })} rowSpan={3} bordered placeholder="اكتب هنا عنوان الشركة بكامل" style={[styles.MyInput,{height:60,}]} />
              </Item>


  </Form>
  </Animatable.View>
</Content>



        </Container>

<View  style={{}}>
<TouchableOpacity style={[styles.buttonContainer,{backgroundColor:c.PrimeryColor}]} onPress={()=> this.UserLoginFunction()}>
       
{this.state.SpinnerS ? 
          <Spinner color='red' />
           : 
           <Text style={styles.textButton} > تسجيل </Text>
        }
           
          </TouchableOpacity>


</View>
</KeyboardAvoidingView>
      </ImageBackground>
      
    );
  }
}

const styles = StyleSheet.create({

  imgBackground: {
    width: '100%',
    height: '100%',
flex:1
  },
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 18,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 10,
    
   
  },
  Label1: {

    color: colors.PrimeryColor,
    textAlign: "right",
    fontFamily: 'Kan',
    width: '100%',
    marginTop: 10,
  },
  MyInput: {
    height: 45,
    textAlign: 'right',
    width: '100%',
    borderColor: '#f0f0f0',
    borderWidth: 1,
    marginTop:10,
    color: colors.PrimeryColor,
    backgroundColor:'#ffff',
    // borderRadius:20,
    paddingHorizontal:20,
    paddingRight:20
    
  },
  Myfrom: {
    borderRadius: 20,
   justifyContent:'center',
   marginRight:20,
    width:'100%'

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 10,
    height: 40,
    marginBottom:20

  }
  , textButton: {
    color: colors.SecondColor,
    fontSize: 12,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
});
