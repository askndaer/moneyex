import React from 'react';
import { StyleSheet, RefreshControl ,AsyncStorage,Modal,Text,Alert, View,BackHandler, ImageBackground,ScrollView, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Drawer,Thumbnail,Icon } from 'native-base';
import SideBar from './MySiderBar';
import colors from './Color';
import Header from './Header';
import MySiderBar from './MySiderBar';
import { AppRegistry, FlatList,  ActivityIndicator, Platform} from 'react-native';
import {  Card, CardItem, Body } from 'native-base';
import { Entypo } from '@expo/vector-icons';
import Moment from 'moment';
const new1 = require("../assets/new.png");
import NumberFormat from "react-number-format";
let isHeader=0;
import axios from "axios";
export default class login extends React.Component {

  
  constructor(props)
  {
     super(props);
     this.state = { 
    isLoading: false,
    UserType:false,
    refreshing: false

  }
  this.props.navigation.addListener('willFocus', (x) => {

      const { navigation } = this.props;
      const Type = navigation.getParam('Type', 'US');
    
    this.fetchData(Type);
  })
  }
    opeToast = () => {
      // alert('fsd'),
      Alert.alert(
        'تنبية',
        'لا يمكنك مشاهدة المحتوى ',
        [
          {text: 'تسجيل الدخول ', onPress: () =>       this.props.navigation.navigate('Login') },
          {text: 'مستخدم جديد', onPress: () =>   this.props.navigation.navigate('RegisterUser'), style: 'cancel'},
          
        ],
        { cancelable: true }
      )
      
      };

      GuestLogin = async  (usa_buy1,usa_sell1,euro_buy1,euro_sell1,company_name1,phone_number1,whatsup_number1,viber_number1,id1,rating1,countrating1,address1)  => {
        const ds = await AsyncStorage.getItem('@MySuperStore:Type');
        if(ds!='false'){
        this.props.navigation.navigate('agintview', {
          usa_buy : usa_buy1,
           usa_sell :usa_sell1,
           euro_buy : euro_buy1,
           euro_sell : euro_sell1,
           company_name : company_name1,
           phone_number : phone_number1,
           whatsup_number : whatsup_number1,
           viber_number : viber_number1,
           id :id1,
           rating:rating1,
           countrating:countrating1,
           address:address1
        });
     

      }else{

     
        this.opeToast();
      }
    

     };


     _onRefresh(){
      this.setState({refreshing: true});
      this.fetchData().then(() =>{
        this.setState({refreshing: false})
      });
    }
    
     componentDidMount() {
     
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    
    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    
    handleBackPress = () => {
      this.props.navigation.navigate('Main','LoginN',
      {
        FromScreen: "View",
        Type: 'US',
      }
      );
      return true;
    }
  
  
 FlatListItemSeparator = () => {
     return (
       <View
         style={{
           height: 1,
           width: "100%",
           backgroundColor: "#607D8B",
         }}
       />
     );
   }
  
 
   cc = () => {


   }

    fetchData = async(Type) =>{
      const instance = axios.create()
      instance.defaults.timeout = 2500;
      instance.post("http://konuze.com/index.php/api/example/getmmoneylist")
      .then(responseJson => {
 
    
      
      let MyResponse = responseJson.data['users'];
    
      var count = Object.keys(MyResponse).length;
      var mydate1 = Moment().format('YYYY-MM-DD');
      var person = { header: true, name: "" };
      for (i = 0; i < count; i++) { 
        
      
      if(Moment(MyResponse[i]['update_time']).format('YYYY-MM-DD').toString() != mydate1.toString()){
      MyResponse.splice(i, 0, person)
      break;
      }
    }
      console.log(MyResponse);
            this.setState({
              isLoading: false,
              dataSource: MyResponse
            }, function () {
              // In this block you can do something with new state.
            });
// }
}).catch((error) => {
  Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
  this.setState({
    SpinnerS: false
  })
});;
  
};
  render() {
    const { navigation } = this.props;
    const FromScreen = navigation.getParam('FromScreen', 'NO');
    const Type = navigation.getParam('Type', 'US');
    if (FromScreen=='Main'){
      // this.fetchData();
  
    // alert('fsdf');
    
    
    }
    CustomProgressBar = ({ visible }) => (
      <Modal onRequestClose={() => null} visible={visible}>
        <View style={{ flex: 1, backgroundColor: '#dcdcdc', alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ borderRadius: 10, backgroundColor: 'white', padding: 25 }}>
            <Text style={{fontFamily: 'Kan', fontSize: 15, fontWeight: '200' }}>جلب البيانات</Text>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <CustomProgressBar />
        </View>
      );
    }


  var mydate1 = Moment().format('YYYY-MM-DD');
  
    return (
      


      <ImageBackground style={styles.imgBackground} ref="view1"
        resizeMode='cover'
        source={require('../assets/main.png')}>

        <Header  navigation={this.props.navigation}  />
  
        <Animatable.View animation="slideInDown" iterationCount={1} direction="alternate" style={styles.TopCon}>
     
     {Type=='US'?
        <Text style={styles.TheH1} > اسعار   الدولار     </Text>
        :
        <Text style={styles.TheH1} > اسعار   اليورو     </Text>
    }
        </Animatable.View>


        <View style={styles.ButtomCon}>

        {/* // TODO: d */}

<View style={{flexDirection:'row-reverse',marginBottom:5}}>

  <Text style={[styles.TextNumber,{flex:3,justifyContent:'center',textAlign:'center',}]} > اسم الشركة</Text>
  <View style={{flex:2,flexDirection:'row'}}>
  <Text  style={[styles.TextNumber,{flex:1,justifyContent:'center',textAlign:'right'}]} >سعر الشراء </Text>
  <Text style={[styles.TextNumber,{flex:1,justifyContent:'center',textAlign:'right'}]} >سعر البيع</Text>
  </View>
</View>


   <FlatList
        
        refreshControl={
          <RefreshControl
          refreshing = {this.state.refreshing}
          onRefresh={this._onRefresh.bind(this)}
          />
        }

       data={ this.state.dataSource }
       
      //  ItemSeparatorComponent = {this.FlatListItemSeparator}

       renderItem={({item}) =>
       <ScrollView style={styles.ScrollContainer}>
      
    
   
      
      {item.header==true?
      <View>
<Text style={[styles.TextNumber2,{}]} > اسعار القديمة</Text> 
      </View>
      :
      
      (item.usa_buy != null && item.usa_sell != null)   &&
     <TouchableOpacity onPress={() => {
       
       this.GuestLogin(item.usa_buy,item.usa_sell,item.euro_buy,item.euro_sell,item.company_name,item.phone_number,item.whatsup_number,item.viber_number,item.id,item.rating,item.countrating,item.address) 
      //  this.GuestLogin(item.id)
          }} >






        <View style={styles.MainBox}>
        { Moment(item.update_time).format('YYYY-MM-DD').toString() == mydate1.toString() ?
        <Image source={new1} style={{width:30,height:30, position:'absolute',right: -2,top:-2,}} resizeMode="contain" />
        :<View></View>
      }

        <Text style={[styles.TextNumber,{flex:3,justifyContent:'center',textAlign:'right'}]} >  {item.company_name}  </Text>
        <View style={{flex:2,flexDirection:'row'}}>
        <Icon name="md-arrow-dropleft" style={{fontSize: 25,alignSelf: "center",color:colors.PrimeryColor}}/>
        {Type=='US'? 
            <View  style={{flex:1,justifyContent:'center',margin:5}}>
            {/* <Text style={[styles.TextNumber2,{flex:1}]} > سعر البيع  </Text> */}
             <View style={{flex:1,flexDirection:'row',justifyContent:'center'}}>
             { item.usa_sell > item.usa_sell_old ?
             <Entypo name="arrow-bold-up" style={{fontSize: 20,alignSelf: "center",color:'#2ed573'}}/>
             :
             <Entypo name="arrow-bold-down" style={{fontSize: 20,alignSelf: "center",color:'#ff3838'}}/>
             }
             <NumberFormat
                      value={item.usa_sell}
                      displayType={"text"}
                      thousandSeparator={true}
                       decimalScale={4}
                      fixedDecimalScale={true}
                      renderText={value => (
                        <Text style={[styles.TextNumber1,{}]} >  {value}  </Text>
                      )}
                    />
             
             </View> 
             </View>
             :
             <View  style={{flex:1,justifyContent:'center',margin:5}}>
             {/* <Text style={[styles.TextNumber2,{flex:1}]} > سعر البيع  </Text> */}
              <View style={{flex:1,flexDirection:'row',justifyContent:'center'}}>
              { item.euro_sell > item.euro_sell_old ?
              <Entypo name="arrow-bold-up" style={{fontSize: 20,alignSelf: "center",color:'#2ed573'}}/>
              :
              <Entypo name="arrow-bold-down" style={{fontSize: 20,alignSelf: "center",color:'#ff3838'}}/>
              }
              <NumberFormat
                      value={item.euro_sell}
                      displayType={"text"}
                      thousandSeparator={true}
                       decimalScale={4}
                      fixedDecimalScale={true}
                      renderText={value => (
                        <Text style={[styles.TextNumber1,{}]} >  {value}  </Text>
                      )}
                    />
              
              </View> 
              </View>
              }








{Type=='US'? 
             <View  style={{flex:1,justifyContent:'center',margin:5}}>
            {/* <Text style={[styles.TextNumber2,{flex:1}]} > سعر الشراء  </Text> */}
             <View style={{flex:2,flexDirection:'row',justifyContent:'center'}}> 
            {  item.usa_buy > item.usa_buy_old?
             <Entypo name="arrow-bold-up" style={{fontSize: 20,alignSelf: "center",color:'#2ed573'}}/>
             :
             <Entypo name="arrow-bold-down" style={{fontSize: 20,alignSelf: "center",color:'#ff3838'}}/>
             }
             <NumberFormat
                      value={item.usa_buy}
                      displayType={"text"}
                      thousandSeparator={true}
                       decimalScale={4}
                      fixedDecimalScale={true}
                      renderText={value => (
                        <Text style={[styles.TextNumber1,{}]} >  {value}  </Text>
                      )}
                    />
                        
             </View> 
             </View>
:
<View  style={{flex:1,justifyContent:'center',margin:5}}>
{/* <Text style={[styles.TextNumber2,{flex:1}]} > سعر الشراء  </Text> */}
 <View style={{flex:2,flexDirection:'row',justifyContent:'center'}}> 
{  item.euro_sell > item.euro_sell_old?
 <Entypo name="arrow-bold-up" style={{fontSize: 20,alignSelf: "center",color:'#2ed573'}}/>
 :
 <Entypo name="arrow-bold-down" style={{fontSize: 20,alignSelf: "center",color:'#ff3838'}}/>
 }
 <NumberFormat
                      value={item.euro_sell}
                      displayType={"text"}
                      thousandSeparator={true}
                       decimalScale={4}
                      fixedDecimalScale={true}
                      renderText={value => (
                        <Text style={[styles.TextNumber1,{}]} >  {value}  </Text>
                      )}
                    />
             
 </View> 
 </View>
}

        </View>
        </View>


    </TouchableOpacity>
      }
</ScrollView>
}
keyExtractor={(item, index) => index.toString()}
/>







        </View>
</ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 15,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
     margin: 5,

  },
  ScrollContainer:{
    flex: 1,
},
  MainBox: {
    flexWrap: 'wrap-reverse',
    flexDirection: 'row-reverse',
    width: '98%',
 
    padding:10,
    margin:5,
    backgroundColor:colors.SecondColor,
    borderWidth:1,
    borderColor:'#dcdcdc'
   },

  Boxs: {
    width: '40%',
    justifyContent: "center",
    alignSelf: "center",
  },

  
  TextTitle: {
    color: colors.SecondColor,
    fontSize: 20,

textAlign:'right',
    // alignSelf:'flex-end',
    fontFamily: 'Kan',

  },


  TextNumber: {
    color: colors.PrimeryColor,
    fontSize: 12,
    fontFamily: 'Kan',
    textAlign:'right'
    
  },

  TextNumber1: {
    color: colors.PrimeryColor,
    fontSize: 16,
    textAlign:'right'
    
  },
  
  TextNumber2: {
    color: colors.PrimeryColor,
    fontSize: 16,
    fontFamily: 'Kan',
    textAlign:'center',
    margin:2,
    
  },
  TextNumber32: {
    color: '#dcdcdc',
    fontSize: 9,
    // fontFamily: 'Kan',
    textAlign:'center',
    margin:2,
    
    
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    flex:1,
  },
  TopCon: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "center",
  },
  ButtomCon: {
    flex: 10,
    margin:5
  }, 
  Footer: {
    flex: 0,
  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '90%',
    borderRadius: 6,
    marginTop: 10,
    height: 40,

  }, textButton: {
    color: colors.SecondColor,
    fontSize: 15,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
  },

  SmallbuttonContainer:{
    // backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '40%',
    borderRadius: 6,
    marginBottom: 10,

    height: 40,
  },
  inputText: {
    color: colors.PrimeryColor,
    fontSize: 15,
    width: '70%',
    backgroundColor: '#ffffff0C',
    flexDirection: 'row',
    paddingVertical: 5,
    borderRadius: 3,
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    width: '70%',
    marginTop: 10,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    textAlign: "center",
    alignSelf: "center",

  },
  TButton: {
    marginTop: 30,
    height: 40,
    textAlign: "center",
    alignSelf: "center",
    color: colors.PrimeryColor,
  },
  logo: {
    width: '60%',
    marginTop: 150,
    alignSelf: "center",

  }
});
