import React from 'react';
import { StyleSheet, Text, View, ImageBackground,AsyncStorage,TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Drawer } from 'native-base';
import SideBar from './MySiderBar';
import colors from './Color';
const menu = require("../assets/icons/menu.png");
import { withNavigation } from 'react-navigation';
import { Entypo,Ionicons } from '@expo/vector-icons';

export default class Header extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            isType: 'false',
       
    
        };
        
    }
    
      componentWillMount() {
     
        this.getKey()
     }
     
    async getKey() {
        try {
            console.log("1111111111111111111111111111111111123123123" );
          const L_isType= await AsyncStorage.getItem('@MySuperStore:Type');
            this.setState({isType: L_isType});
            console.log(L_isType);
        } catch (error) {
          console.log("Error retrieving data" + error);
        }
      }

      navigateToScreen (){
        this.props.navigation.goBack();
      }
      
      

  render() {
    return (


    <View style={{marginBottom:10, height:80,backgroundColor:c.PrimeryColor,justifyContent:'flex-end',flexDirection:"row"}}>
    <View  style={styles.HeaderIcon} >
    {this.state.isType=='false' ?
<TouchableOpacity style={styles.Boxs1}  onPress={() => {this.props.navigation.goBack(); }}>
{/* <Image source={menu} style={styles.MenuItemImg} resizeMode="contain" /> */}
 <Ionicons name="ios-arrow-back" size={32} color={colors.SecondColor} />
</TouchableOpacity>
:<View></View>
}
 {this.props.back=='true' ?
<TouchableOpacity style={styles.Boxs1}  onPress={() => {this.props.navigation.navigate('Pricelist'); }}>
{/* <Image source={menu} style={styles.MenuItemImg} resizeMode="contain" /> */}
 <Ionicons name="ios-arrow-back" size={32} color={colors.SecondColor} />
</TouchableOpacity>
:<View></View>
}

</View>
    <Text style={styles.HeaderTEXT} >الغيث</Text>

<View  style={styles.HeaderIcon} >
{this.state.isType!='false' ?
<TouchableOpacity style={styles.Boxs}  onPress={() => { this.props.navigation.openDrawer() }}>
{/* <Image source={menu} style={styles.MenuItemImg} resizeMode="contain" /> */}
 <Entypo name="menu" size={32} color={colors.SecondColor} />
</TouchableOpacity>
:
<TouchableOpacity style={styles.Boxs} ></TouchableOpacity>

}
</View>



            
    </View>
    );
}
}

const styles = StyleSheet.create({
 
  
    MenuItemImg:{
  marginTop:20,
height:27,
width:27,


textAlign: "center",
alignSelf: "center",
    },
    Boxs1:{
      
      justifyContent:'flex-end',
      alignSelf:'flex-end',
     marginLeft:10,
    },
    HeaderTEXT:{
      
        // flex:1,
   
        color:colors.SecondColor,
        fontSize: 20,
        fontFamily: 'Kan',
        margin: 10,
        flex:1,
        width:'100%',
        alignSelf:'flex-end',
        alignItems: 'center',
        margin: 5,
        textAlign:'right'
            },

        HeaderIcon:{
        // flex:1,
alignSelf:'flex-end',
alignItems: 'flex-end',
margin: 5,
       
    },


  });
  