import React from 'react';
import { StyleSheet, Modal,Text,Alert, View,AsyncStorage,BackHandler, ImageBackground,ScrollView, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Drawer,Thumbnail,Icon } from 'native-base';
import {Divider} from 'react-native-elements'
import {Linking} from 'react-native'

import Rating from 'react-native-rating';
import { Easing } from 'react-native';
import call from 'react-native-phone-call';
import SideBar from './MySiderBar';
import colors from './Color';
import Header from './Header';
import MySiderBar from './MySiderBar';
const star = require("../assets/star.png");
import { Dialog } from 'react-native-simple-dialogs';
import Config from './Config'
import {  Spinner } from 'native-base';
c = new Config


import { AppRegistry, FlatList,  ActivityIndicator, Platform} from 'react-native';

const images = {
  starFilled: require('../assets/star_filled.png'),
  starUnfilled: require('../assets/star_unfilled.png')
}

const openApp = async (url, iosLink, androidLink) => {
  try {
    return await Linking.openURL(url);
  } catch(err) {
    const urlMarket = Platform.OS === 'ios'
      ? iosLink
      : androidLink;
    return await Linking.openURL(urlMarket);
  }
}
export default class login extends React.Component {

  
  constructor(props)
  {
     super(props);
     this.state = { 
     isLoading: true,
     company_id:'',
     usa_buy:0,
     usa_sell:0,
     euro_buy:0,
     euro_sell:0,
     create_at:0,
     note:'',
     company_name:'',
     phone_number: '',
     whatsup_number:"" ,
     viber_number:'' ,
     dialogVisible: false,
     setRating:0,
     RatinigView:0,
     SpinnerS:false,
  }
  
  this.props.navigation.addListener('willFocus', (x) => {
    // const { navigation } = this.props;
    // const usa_buy = navigation.getParam('usa_buy', '0');
    // const usa_sell = navigation.getParam('usa_sell', '0');
    // const euro_buy = navigation.getParam('euro_buy', '0');
    // const euro_sell = navigation.getParam('euro_sell', '0');
    // const company_name = navigation.getParam('company_name', '0');
    // const phone_number = navigation.getParam('phone_number', '0');
    // const whatsup_number = navigation.getParam('whatsup_number', '0');
    // const viber_number = navigation.getParam('viber_number', '0');
    // const id = navigation.getParam('id', '0');
    // const rating = navigation.getParam('rating', null);
    // const countrating = navigation.getParam('countrating', '0');
    // this.setState({RatinigView:0})
    this.setState({RatinigView:0})
    
})
}
    

componentDidMount() {
  BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
}

componentWillUnmount() {
  BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
}

handleBackPress = () => {
  this.props.navigation.navigate('Pricelist',
  {
    FromScreen: "View",
    Type: 'US',
  }
  );
  return true;
}
  
  

  GetDataFromServer = async (x) => {

   
  //  if (FromScreen=='Main')
    let Mybody="id = ".x;
   
    fetch('http://konuze.com/index.php/api/example/getmmoneylistcompany', {

      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
      }),
      body:Mybody

    }).then((response) => response.json())

    .then((responseJson) => {
 
      // console.log("11111111111111111111111111111111111111111");
      console.log(responseJson['users']);
      let MyResponse = responseJson['users'];
      console.log("ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc");
      console.log(MyResponse[0].company_name);
  
      this.setState({usa_buy: MyResponse[0].usa_buy});
      this.setState({usa_sell: MyResponse[0].usa_sell});
      this.setState({euro_buy: MyResponse[0].euro_buy});
      this.setState({euro_sell: MyResponse[0].euro_sell});
      this.setState({company_name: MyResponse[0].company_name});
      this.setState({phone_number: MyResponse[0].phone_number});
      this.setState({whatsup_number: MyResponse[0].whatsup_number});
      this.setState({viber_number: MyResponse[0].viber_number});
     }).catch((error) => {
      Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
     
    });

    

  
  }
  callNumber  (x) {

const args = {
  number: x,
  prompt: false,
};
call(args).catch(console.error);
  }

  callwhatsup_number (x)  {
   
    let url = 'whatsapp://send?text=' + 'السلام عليكم' + '&phone=' + x;
        Linking.openURL(url).then((data) => {
          console.log('WhatsApp Opened');
        }).catch(() => {
          alert('Make sure Whatsapp installed on your device');
        });
   
  }

  callViber_number  (x)  {
    const args = {
      number: x,
      prompt: false,
    };
    if (!args.number) { return Promise.reject('no number provided') }
 
    openApp(
      `viber://add?number=${args.number}`,
      'itms-apps://itunes.apple.com/us/app/id382617920?mt=8',
      'market://details?id=com.viber.voip'
    );
    
    

  }
  

  newComp(company_name,id) {
    this.props.navigation.navigate('NewComp',
    {
      id: id,
      company_name: company_name,
    }
    );
  }
 

  _securePhrase = async () => {
    this.setState({ dialogVisible: true });
};

_securePhraseOk = async () => {
  this.setState({
    SpinnerS: true
  })
  const { navigation } = this.props;
  const id = navigation.getParam('id', '0');
  const client_id = await AsyncStorage.getItem('@MySuperStore:id');
  
   let Mybody=
  "company_id="+ id+"&"+
  "client_id="+ client_id+"&"+
    "rating="+ this.state.setRating+"";
    
  fetch('http://konuze.com/index.php/api/example/insertrating', {

    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
    }),
    body:Mybody

  }).then((response) => response.json())

  .then((responseJson) => {

    console.log("11111111111111111111111111111111111111111");
    console.log(responseJson['users']);
    if(responseJson['users']==true)
    this.setState({RatinigView:1})
    else
    this.setState({RatinigView:2})
    this.setState({
      SpinnerS: false
    })
   }).catch((error) => {
    Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
   
  });

  

    // this.setState({ dialogVisible: false });
   
};

  render() {
    
    const { navigation } = this.props;
    const usa_buy = navigation.getParam('usa_buy', '0');
    const usa_sell = navigation.getParam('usa_sell', '0');
    const euro_buy = navigation.getParam('euro_buy', '0');
    const euro_sell = navigation.getParam('euro_sell', '0');
    const company_name = navigation.getParam('company_name', '0');
    const phone_number = navigation.getParam('phone_number', '0');
    const whatsup_number = navigation.getParam('whatsup_number', '0');
    const viber_number = navigation.getParam('viber_number', '0');
    const id = navigation.getParam('id', '0');
    const rating = navigation.getParam('rating', null);
    const countrating = navigation.getParam('countrating', '0');
    const address = navigation.getParam('address', '0');
    

        return (
      <ImageBackground style={[styles.imgBackground,{}]} ref="view1"
        resizeMode='cover'
        source={require('../assets/main.png')}>

      <Header  navigation={this.props.navigation}  back={'true'}/>


      <Dialog  dialogStyle={{backgroundColor:'#fff'}}
                                // title="Is this your secure phrase?"
                                visible={this.state.dialogVisible}
                                onTouchOutside={() => this.setState({ dialogVisible: false })}
                               >
                                <Animatable.View style={{borderRadius:20}} ref="view1">
                                <Text style={{fontFamily: 'Kan',  fontSize: 14,justifyContent:'center',textAlign:'center'}}>تقييم الشركة</Text>
                                <Divider style={{ backgroundColor: '#f0f0f0' ,margin:10,}} />
                               <View style={{justifyContent:'center',alignContent:'center',alignSelf:'center'}} >
                                {this.state.RatinigView==0?
                                <Rating
initial={0}

    onChange={rating => 
    this.setState({setRating:rating})
    }
    selectedStar={images.starFilled}
    unselectedStar={images.starUnfilled}
    config={{
      easing: Easing.inOut(Easing.ease),
      duration: 350
    }}
    stagger={80}
    maxScale={1.4}
    starStyle={{
      width: 40,
      height: 40
    }}
  />
  :
  <View>
    {this.state.RatinigView == 1 ?
    <Text style={[styles.TheH5,{}]}>تم التقييم بنجاح</Text>
  :
  <Text style={[styles.TheH5,{}]}>لقد تم التقييم مسبقا</Text>  
  }
  </View>
  }
  </View>
                                <Divider style={{ backgroundColor: '#f0f0f0' ,margin:10,}} />
                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                <TouchableOpacity  style={[styles.buttonContainer1,{backgroundColor:c.PrimeryColor}]} onPress={() => this.setState({ dialogVisible: false })}>
                                <Text style={styles.textButton} >الغاء</Text>
          </TouchableOpacity>
       
       {this.state.RatinigView == 0 ?
          <TouchableOpacity  style={[styles.buttonContainer1,{backgroundColor:c.PrimeryColor}]} onPress={() => this._securePhraseOk()}>
         {this.state.SpinnerS ? 
          <Spinner color='red' />
           : 
           <Text style={styles.textButton} >تقييم </Text>
        }
        
          </TouchableOpacity>
       :
       <TouchableOpacity  style={[styles.buttonContainer1,{backgroundColor:c.PrimeryColor}]} onPress={() => this.setState({ dialogVisible: false })}>
       <Text style={styles.textButton} >اخفاء</Text>
</TouchableOpacity>
       }
          </View>
                                </Animatable.View>
                            </Dialog >

      <ScrollView>
      <Animatable.View animation="slideInDown" iterationCount={1} direction="alternate" style={styles.TopCon}>

       <Text style={styles.TheH1} >
       {company_name}
       
      
       </Text>
<View style={{justifyContent:'center',alignContent:'center',alignSelf:'center'}}>
       

 <TouchableOpacity style={[{backgroundColor:c.PrimeryColor,width:60,height:60,borderRadius:30,justifyContent:'center',alignContent:'center'}]} onPress={this._securePhrase }>
           
            <Text style={{color:'#fff',textAlign:'center'}} >5/{rating}</Text>
           
            </TouchableOpacity>


       </View>
       <Text style={[styles.TheH3,{fontSize:12}]} >
        عدد الالمقيمين {countrating}
       </Text>

       <View style={{flexDirection:'row-reverse',justifyContent:'space-between',marginTop:30 ,width:'100%'}}>

        <Text style={[styles.TheH2,{flex:2,}]}>العملة</Text>
        <Text style={[styles.TheH5,{flex:1,}]}>البيع</Text>
        <Text style={[styles.TheH5,{flex:1,}]}>الشراء</Text>
        </View>
        <Divider style={{ backgroundColor: colors.PrimeryColor ,marginTop:10}} />

        <View style={{flexDirection:'row-reverse',justifyContent:'space-between' ,width:'100%'}}>
        <Text style={[styles.TheH2,{flex:2,}]}>الدولار</Text>
        <Text style={[styles.TheH5,{flex:1,}]}>{usa_buy}</Text>
        <Text style={[styles.TheH5,{flex:1,}]}>{usa_sell}</Text>
        </View>
        <Divider style={{ backgroundColor: colors.PrimeryColor ,marginTop:10}} />

        
        <View style={{flexDirection:'row-reverse',justifyContent:'space-between' ,width:'100%'}}>
        <Text style={[styles.TheH2,{flex:2,}]}>اليورو</Text>
        <Text style={[styles.TheH5,{flex:1,}]}>{euro_buy}</Text>
        <Text style={[styles.TheH5,{flex:1,}]}>{euro_sell}</Text>
        </View>
        <Divider style={{ backgroundColor: c.PrimeryColor ,marginTop:10}} />

<View style={{marginTop:10}}>
         <TouchableOpacity style={[styles.buttonContainer,{backgroundColor:c.PrimeryColor}]} onPress={this.callNumber.bind(this,phone_number) }>
            <Text style={styles.textButton} >اتصل الان</Text>
            </TouchableOpacity>

           <View style={{flexDirection:'row',justifyContent:'space-between'}}>
             <TouchableOpacity style={[styles.buttonContainer1,{backgroundColor:c.PrimeryColor}]} onPress={this.callwhatsup_number.bind(this,whatsup_number) }>
            <Text style={styles.textButton} >اتصل وتس اب</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.buttonContainer1,{backgroundColor:c.PrimeryColor}]} onPress={this.callViber_number.bind(this,viber_number) }>
            <Text style={styles.textButton} >اتصل فيبر</Text>
            </TouchableOpacity>
            </View>

            <TouchableOpacity style={[styles.buttonContainer,{backgroundColor:c.PrimeryColor}]} onPress={this.newComp.bind(this,company_name,id)}>
            <Text style={styles.textButton} > شكوى جديدة</Text>
            </TouchableOpacity>
            </View> 

<View style={{margin:30}}>
<Text style={styles.TheH3} >العنوان</Text>
<Text style={[styles.TheH2,{}]}>{address}</Text>

</View>
      

                    </Animatable.View>

       
                    </ScrollView>

      </ImageBackground>
    
    );
  }
}

const styles = StyleSheet.create({
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 25,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    // margin: 5,

  },
  TheH2: {
    color: colors.PrimeryColor,
    fontSize: 18,
    textAlign: "right",
    // alignSelf: "center",
    fontFamily: 'Kan',
    margin: 10,

  },
  TheH3: {
    color: colors.PrimeryColor,
    fontSize: 18,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 10,

  },TheH5: {
    color: colors.PrimeryColor,
    fontSize: 18,
    textAlign: "center",
    alignSelf: "center",
    // fontFamily: 'Kan',
    margin: 10,
    // borderRadius: 60/2,
    // borderColor:colors.SecondColor,

  },
  TheMainBox: {
    backgroundColor: colors.PrimeryColor,
        // backgroundColor:'#ecf0f1',
    marginTop:5,
    borderRadius: 10,
marginLeft: 20,
marginRight: 20,
borderWidth:2,
  },
  MainBox: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: "flex-end",
    height:80,
    padding:20,

  },

  Boxs: {
    width: '40%',
    // height: '10%',
    justifyContent: "center",
    alignSelf: "center",
    
  },
  MainTitle: {
    backgroundColor:'#fff',
    color:colors.PrimeryColor,
    fontSize: 20,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    marginBottom:5,
    padding:10,
    marginTop:-30,
    borderRadius: 20,
    borderColor:colors.PrimeryColor,
  
    borderWidth:2,
  },
  TextTitle: {
    color: colors.PrimeryColor,
    fontSize: 20,

textAlign:'right',
    // alignSelf:'flex-end',
    fontFamily: 'Kan',

  },


  TextNumber: {
    color: colors.PrimeryColor,
    fontSize: 20,
    fontFamily: 'Kan',
    
  },

  imgBackground: {
    width: '100%',
    height: '100%',
    flex:1,
  },
  TopCon: {
    flex: 1,
    justifyContent: 'flex-start',
    alignSelf: "center",
    margin:20
  },
  ButtomCon: {
    flex: 10,

  },
  Footer: {
    flex: 0,

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '100%',
    borderRadius: 6,
    margin: 10,
    height: 40,

  } , buttonContainer1: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '45%',
    borderRadius: 6,

    // margin: 10,
    height: 40,

  }, textButton: {
    color: colors.SecondColor,
    fontSize: 15,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    width:"100%"
    
  },

  SmallbuttonContainer:{
    // backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '40%',
    borderRadius: 6,
    marginBottom: 10,

    height: 40,
  },
  inputText: {
    color: colors.PrimeryColor,
    fontSize: 15,
    width: '70%',
    backgroundColor: '#ffffff0C',
    flexDirection: 'row',
    paddingVertical: 5,
    borderRadius: 3,
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    width: '70%',
    marginTop: 10,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    textAlign: "center",
    alignSelf: "center",

  },
  TButton: {
    marginTop: 30,
    height: 40,
    textAlign: "center",
    alignSelf: "center",
    color: colors.PrimeryColor,
  },
  logo: {
    width: '60%',
    marginTop: 150,
    alignSelf: "center",

  }
});
