import * as ImagePicker from 'expo-image-picker';
import React from 'react';
import { ActivityIndicator, AsyncStorage, Clipboard, Image, ImageBackground, Share, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { NavigationActions } from 'react-navigation';
import colors from './Color';
import Config from './Config';



const profile = require("../assets/icons/profile.png");
const password = require("../assets/icons/password.png");
const contact = require("../assets/icons/contact.png");
const about = require("../assets/icons/about.png");
const logout = require("../assets/icons/logout.png");

c = new Config


export default class MySiderBar extends React.Component {

    constructor() {
        super();
        this.state = {
            Email: 'No Email',
            Name: 'No Name',
            Phone: 'No Phone',
            Type:'',
            uri1:"http://style.anu.edu.au/_anu/4/images/placeholders/person.png",
            image: null,
            uploading: false,
            id:'9'
        }
        this.getKey();
    }

    navigateToScreen = (route) => () => {

        const navigateAction = NavigationActions.navigate({
            routeName: route,
            header: null,
        });
        this.props.navigation.dispatch(navigateAction, {id:9,date: new Date()});
        
    }

 
    logout1 = async () => {
        try {
            await AsyncStorage.removeItem('userToken');

        } catch (error) {
            console.log("Error saving data" + error);
        }
        this.props.navigation.navigate('LogOutN');
    };


    async getKey() {
        try {
            const id = await AsyncStorage.getItem('@MySuperStore:id');
            const L_Email = await AsyncStorage.getItem('@MySuperStore:Email');
            const L_Name = await AsyncStorage.getItem('@MySuperStore:Name');
            const L_Phone = await AsyncStorage.getItem('@MySuperStore:photo');
            const L_Type = await AsyncStorage.getItem('@MySuperStore:Type');
            this.setState({ Email: L_Email });
            this.setState({ Name: L_Name });
            this.setState({ Phone: L_Phone });
            this.setState({ Type: L_Type });
            this.setState({ id: id });
      //  uri='http://konuze.com/assets/images//client/'.Phone;
      console.log('2222222222222222222222222222222222222222222222222222222222222222'.L_Type);
      if(L_Type != 'company')
      this.setState({ uri1: "http://konuze.com/assets/images//client/"+L_Phone });
     
       else{
        this.setState({ uri1: "http://konuze.com/assets/images/"+L_Phone });
   
    //    alert(uri);
       
    }
      console.log(L_Phone);
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }


    




    _maybeRenderUploadingOverlay = () => {
        if (this.state.uploading) {
          return (
            <View
              style={[
                StyleSheet.absoluteFill,
                {
                  backgroundColor: 'rgba(0,0,0,0.4)',
                  alignItems: 'center',
                  justifyContent: 'center',
                },
              ]}>
              <ActivityIndicator color="#fff" animating size="large" />
            </View>
          );
        }
      };
    
      _maybeRenderImage = () => {
        let { image } = this.state;
        if (!image) {
          return;
        }
    
        return (
         
    

            <View >
                      <Image source={{ uri: image }} 
              style={{ marginTop: 40, width: 90, height: 90, borderRadius: 90/2}} />
                    <Text style={[styles.MenuItemTEXT1, { flex: 1 }]}  >{this.state.Name}</Text>
                </View>

        );
      };
    
      _share = () => {
        Share.share({
          message: this.state.image,
          title: 'Check out this photo',
          url: this.state.image,
        });
      };
    
      _copyToClipboard = () => {
        Clipboard.setString(this.state.image);
        alert('Copied image URL to clipboard');
      };
    
      _takePhoto = async () => {
        let pickerResult = await ImagePicker.launchCameraAsync({
          allowsEditing: true,
          aspect: [4, 3],
        });
    
        this._handleImagePicked(pickerResult);
      };
    
      _pickImage = async () => {
        let pickerResult = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
        });
    
        this._handleImagePicked(pickerResult);
      };
    
      _handleImagePicked = async pickerResult => {
        let uploadResponse, uploadResult;
    
        try {
          this.setState({ uploading: true });
    
          if (!pickerResult.cancelled) {
            uploadResponse = await uploadImageAsync(pickerResult.uri,this.state.id,this.state.Type);
            console.log('222222222222222222222222222222222222222222222222222222222222222222222');
           
            let res = uploadResponse.text();
            console.log(res);
            let res1 = res._55.toString().split("<>");
            console.log(res1[1]);
            if(this.state.Type != 'company')
            this.setState({ uri1: "http://konuze.com/assets/images//client/"+res1[1] });
                        else
              this.setState({ uri1: "http://konuze.com/assets/images/"+res1[1] });
         
          }
        } catch (e) {
          console.log({ uploadResponse });
          console.log({ uploadResult });
          console.log({ e });
          alert('Upload failed, sorry :(');
        } finally {
          this.setState({ uploading: false });
        }
      };
    
    










    render() {
        let { image } = this.state;
        return (
            <ImageBackground style={styles.imgBackground}
                resizeMode='cover'
                source={require('../assets/slider.png')}>
                 
                  <TouchableOpacity style={styles.MenuHeader} onPress={this._pickImage}>
                <View >
                    {/* <Thumbnail circle='true' style={{ flex: 3, width: 150, height: 150, textAlignVertical: "bottom", marginTop: 60, }} large source={{ uri: uri }} /> */}
                    <Image source={{ uri: this.state.uri1 }} 
              style={{ marginTop: 40, width: 90, height: 90, borderRadius: 90/2}} />
                    <Text style={[styles.MenuItemTEXT1, { flex: 1 }]}  >{this.state.Name}</Text>
                </View>
                </TouchableOpacity>
                
        {/* {this._maybeRenderUploadingOverlay()} */}
                <Animatable.View style={[styles.MenuBody]} ref="view">
                { (this.state.Type !='company') ?
      
                    <TouchableOpacity style={[styles.MenuItem,{borderColor: c.PrimeryColor,}]} onPress={this.navigateToScreen('Main')}>
                        <Text style={[styles.MenuItemTEXT,{borderColor: c.PrimeryColor,}]} > بيع وشراء العملات</Text>
                        <Image source={profile} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>
                    :
                    <View>
                    <TouchableOpacity style={[styles.MenuItem,{borderColor: c.PrimeryColor,}]} onPress={this.navigateToScreen('MainCompany')}>
                    <Text style={[styles.MenuItemTEXT,{borderColor: c.PrimeryColor,}]} >الرئيسية</Text>
                    <Image source={profile} style={styles.MenuItemImg} resizeMode="contain" />
                </TouchableOpacity>
                <TouchableOpacity style={[styles.MenuItem,{borderColor: c.PrimeryColor,}]} onPress={this.navigateToScreen('Main')}>
                        <Text style={[styles.MenuItemTEXT,{borderColor: c.PrimeryColor,}]} > بيع وشراء العملات</Text>
                        <Image source={profile} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>
                    </View>
                    }
                    <TouchableOpacity style={[styles.MenuItem,{borderColor: c.PrimeryColor,}]} onPress={this.navigateToScreen('passwordChange')}>
                        <Text style={[styles.MenuItemTEXT,{borderColor: c.PrimeryColor,}]} >تحديث كلمة المرور</Text>
                        <Image source={password} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.MenuItem,{borderColor: c.PrimeryColor,}]} onPress={this.navigateToScreen('mesList')}>
                    <Text style={[styles.MenuItemTEXT,{borderColor: c.PrimeryColor,}]} >مراسلة الادارة</Text>
                        <Image source={password} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>

                    

                    { (this.state.Type !='company') ?
                    <TouchableOpacity style={[styles.MenuItem,{borderColor: c.PrimeryColor,}]} onPress={this.navigateToScreen('comList')}>
                        <Text style={[styles.MenuItemTEXT,{borderColor: c.PrimeryColor,}]} >الشكاوي  </Text>
                        <Image source={password} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>
 : <View></View>}


                    <TouchableOpacity style={[styles.MenuItem,{borderColor: c.PrimeryColor,}]} onPress={this.navigateToScreen('aboutus')}>
                        <Text style={[styles.MenuItemTEXT,{borderColor: c.PrimeryColor,}]} >عن التطبيق</Text>
                        <Image source={about} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>

                </Animatable.View>

                <View style={styles.MenuFooter}>
                    <TouchableOpacity style={[styles.MenuItem,{borderColor: c.PrimeryColor,}]} onPress={this.logout1} >
                        <Text style={[styles.MenuItemTEXT,{borderColor: c.PrimeryColor,}]} >تسجيل الخروج</Text>
                        <Image source={logout} style={styles.MenuItemImg} resizeMode="contain" />
                    </TouchableOpacity>
                </View>

            </ImageBackground>
        );
    }








    
}

const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1,
    },

    MenuItem: {
        color: c.PrimeryColor,
        fontSize: 12,
        width: '100%',
        backgroundColor: '#ffffff0C',
        flexDirection: 'row',
        paddingVertical: 5,
        borderRadius: 3,
        borderColor: c.PrimeryColor,
        borderWidth: 1,
        marginTop: 10,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        textAlign: "center",
        alignSelf: "center",
    },

    MenuItemImg: {
        height: 30,
        width: 30,
        margin: 5,
        flex: 1,
        justifyContent: 'flex-end',
    },

    MenuItemTEXT: {
        
        justifyContent: 'center',
        flex: 5,
         textAlignVertical: "center",
        color: colors.SecondColor,
        fontSize: 14,
        fontFamily: 'Kan',
        textAlign:'right',
        alignSelf:'center',
        
        alignItems: 'center' 

    },

    MenuItemTEXT1: {
       
        flex: 5,
        width:'100%',
        // backgroundColor:'#000',
        color: colors.SecondColor,
        fontSize: 14,
        fontFamily: 'Kan',
alignSelf:'center',
    },
    MenuHeader: {
        flex: 3,
        justifyContent: 'center', alignSelf: 'center'
    },

    MenuBody: {
        flex: 8,
    },

    MenuFooter: {
        flex: 1,
    }

});



async function uploadImageAsync(uri,id,type) {
    // let apiUrl = 'https://file-upload-example-backend-dkhqoilqqn.now.sh/upload';
     let apiUrl = 'http://konuze.com/assets/upload.php';

    
    // Note:
    // Uncomment this if you want to experiment with local server
    //
    // if (Constants.isDevice) {
    //   apiUrl = `https://your-ngrok-subdomain.ngrok.io/upload`;
    // } else {
    //   apiUrl = `http://localhost:3000/upload`
    // }
  
    let uriParts = uri.split('.');
    let fileType = uri[uri.length - 1];
  
    let formData = new FormData();
    formData.append('photo', {
      uri,
      name: `photo.${fileType}`,
      type: `image/${fileType}`,
    });
    formData.append('message',id)
    formData.append('type',type)
    
    let options = {
      method: 'POST',
      body: formData,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    };
  
    return fetch(apiUrl, options);
  }