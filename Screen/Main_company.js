import React from 'react';
import { StyleSheet, Button,Modal,BackHandler, Text, View, Alert, AsyncStorage,ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Drawer, Thumbnail } from 'native-base';
import MySiderBar from './MySiderBar';
import colors from './Color';
import Header from './Header';
import { DrawerNavigator } from "react-navigation";
import {  ActivityIndicator } from 'react-native';
import { Spinner } from 'native-base';
// import { Switch } from 'react-native-switch';
import MySwitch from './component/SwitchButton'
import { Item, Input, Label } from 'native-base';
import SwitchSelector from 'react-native-switch-selector';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { ScrollView } from 'react-native-gesture-handler';
import axios from "axios";
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
const options = [
  { label: 'مغلق', value: '1' },
 { label: 'فتح', value: '2' },

];
import * as Localization from 'expo-localization';

export default class Main extends React.Component {


  constructor(props) {

    super(props);
      this.state = {
      BuyUSD: '',
      SellUSD: '',
      BuyUo: '',
      SellUo: '',
      SpinnerS: false,
      SpinnerS1: false,
      activeSwitch: false,


      is_BuyUSD:1,
      is_SellUSD:1,
      is_BuyUo:1,
      is_SellUo:1,
      update_time:'0',
      company_id:20000,
      company_status:'0'
    };

    
   }
   
   async updateToken(){
    console.log("time",Localization.timezone)
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
  
    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
  
    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
      return;
    }
  
    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();
  
    console.log(token)
    const id =    await AsyncStorage.getItem('@MySuperStore:id');
  
    const instance = axios.create()
    instance.defaults.timeout = 2500;
    instance.post("http://konuze.com/index.php/api/example/token_update_company","id="+id+"&token="+token)
    .then(responseJson => {
        console.log(responseJson.data)
    });
  }


GetDataFromServer = async () => {
  const instance = axios.create()
  instance.defaults.timeout = 2500;
 
  this.setState({
    SpinnerS: true
  })
  const id =    await AsyncStorage.getItem('@MySuperStore:id');
  instance.post("http://konuze.com/index.php/api/example/get_companyinformation","company_id="+id)
  .then(responseJson => {
   
      this.setState({
        SpinnerS: false
      })
      
      console.log("1111111111111111111111111111111111ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss1111111");

      // console.log(responseJson);
      let MyResponse = responseJson.data['campanies'][0];
      console.log(MyResponse);
      // if (responseJson.data==1){
     
          this.setState({BuyUSD: MyResponse.usa_buy});
          this.setState({SellUSD:  MyResponse.usa_sell});
          this.setState({BuyUo:  MyResponse.euro_buy});
          this.setState({SellUo:  MyResponse.euro_sell});
 
    
          this.setState({ is_BuyUSD:  MyResponse.is_dollar_buy });
          this.setState({ is_SellUSD:  MyResponse.is_dollar_sell });
          this.setState({ is_BuyUo:  MyResponse.is_euro_buy });
          this.setState({ is_SellUo:  MyResponse.is_euro_sell });
          this.setState({ update_time:  MyResponse.update_time });
          this.setState({ company_status:  MyResponse.status });
          
          
      // }else{
      //   Alert.alert("تنبية", MyResponse.message);
      // }
    //  }) 
    })
    .catch(error => {
      Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
        this.setState({
          SpinnerS: false
        })
    });
  
}




GuestLogin = async  ()  => {
   
  await AsyncStorage.setItem('@MySuperStore:id', "200000000");
  console.log("11111111111111111111111111111111111111111");
  this.props.navigation.navigate('Main');
};

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    this.GetDataFromServer();
   

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    // this.GetDataFromServer();
  }

componentDidMount(){
  this.updateToken();
}

  _is_BuyUSD = (v) => {
    if(v=='1'){
      this.setState({ is_BuyUSD: 0 })
      }else{
      this.setState({ is_BuyUSD: 1 })
      }
  };
  _is_SellUSD = (v) => {
    if(v=='1'){
      this.setState({ is_SellUSD: 0 })
      }else{
      this.setState({ is_SellUSD: 1 })
      }
  };
  _is_BuyUo= (v) => {
    if(v=='1'){
      this.setState({ is_BuyUo: 0 })
      }else{
      this.setState({ is_BuyUo: 1 })
      }
  };
  _is_SellUo= (v) => {
    if(v=='1'){
      this.setState({ is_SellUo: 0 })
      }else{
      this.setState({ is_SellUo: 1 })
      }
  };




  UpdatePrice = async () => {
   
    // BuyUSD: '',
    // SellUSD: '',
    // BuyUo: '',
    // SellUo: '',
    // SpinnerS: false,
    // activeSwitch: false,
    // is_dollars:0,
    // is_euro:0,

    // is_BuyUSD:0,
    // is_SellUSD:0,
    // is_BuyUo:0,
    // is_SellUo:0,
  
    

    const { BuyUSD } = this.state;
    const { SellUSD } = this.state;
    const { BuyUo } = this.state;
    const { SellUo } = this.state;

    const { is_BuyUSD } = this.state;
    const { is_SellUSD } = this.state;
    const { is_BuyUo } = this.state;
    const { is_SellUo } = this.state;
  

 
        if (is_BuyUSD == 0)
        setTimeout(() =>    this.setState({ BuyUSD: '' }));    
             else{
               if(BuyUSD==''){
               alert('يرجاء ادخال سعر البيع الدولار')
              return; 
              }
             }
        if (is_SellUSD == 0)
        setTimeout(() =>    this.setState({ SellUSD: '' }));   
            else{
              if(SellUSD==''){
              alert('يرجاء ادخال سعر الشراء الدولار')
             return; 
             }
            }     
    

   
        if (is_BuyUo == 0)
        setTimeout(() =>    this.setState({ BuyUo: '' }));    
             else{
               if(BuyUo==''){
               alert('يرجاء ادخال سعر البيع اليورو')
              return; 
              }
             }
        if (is_SellUo == 0)
        setTimeout(() =>    this.setState({ SellUo: '' }));   
            else{
              if(SellUo==''){
              alert('يرجاء ادخال سعر الشراء اليورو')
             return; 
             }
            }     
      
      const id =    await AsyncStorage.getItem('@MySuperStore:id'); 
  console.log("Error2222222222222222222222222222222222222"  );
  let Mybody="company_id="+ id+"&"+
              "usa_buy="+ this.state.BuyUSD+"&"+
              "usa_sell="+ this.state.SellUSD+"&"+
              "euro_buy="+ this.state.BuyUo+"&"+
           

              "is_dollar_buy="+ this.state.is_BuyUSD+"&"+
              "is_dollar_sell="+ this.state.is_SellUSD+"&"+
              "is_euro_buy="+ this.state.is_BuyUo+"&"+
              "is_euro_sell="+ this.state.is_SellUo+"&"+

              "euro_sell="+ this.state.SellUo+"";

           

        console.log(Mybody);
    this.setState({
      SpinnerS1: true
    })
    // fetch('http://192.168.0.136/2/application/index.php/api/example/moneyinsert', {
    fetch('http://konuze.com/index.php/api/example/moneyinsert', {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
        }),
        body: Mybody
      }).then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          SpinnerS1: false
        })
        console.log(responseJson);
        let MyResponse = responseJson[0];
        if (MyResponse.response==1){
            this.setState({ update_time:  MyResponse.update_time });
          Alert.alert("تنبية", MyResponse.message);
        }else{
          Alert.alert("تنبية", MyResponse.message);
        }
       }).catch((error) => {
        console.log(error);
        Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
        this.setState({
          SpinnerS1: false
        })
      });
    }
 
  








  render() {

    CustomProgressBar = ({ visible }) => (
      <Modal onRequestClose={() => null} visible={visible}>
        <View style={{ flex: 1, backgroundColor: '#dcdcdc', alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ borderRadius: 10, backgroundColor: 'white', padding: 25 }}>
            <Text style={{   fontFamily: 'Kan',fontSize: 15, fontWeight: '200' }}>جلب البيانات</Text>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
    if (this.state.SpinnerS) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <CustomProgressBar />
        </View>
      );
    }

    return (

      <ImageBackground style={styles.imgBackground} ref="view1"
        resizeMode='cover'
        source={require('../assets/main.png')}>

        <Header navigation={this.props.navigation} />
        {this.state.company_status=='1' ? 
<ScrollView style={{flex:1}}>
        <Text style={styles.TheH1} >
        
          أسعار العملات
          </Text>

        <View style={{  margin: 10, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flex: 1, alignItems:'center',backgroundColor: c.PrimeryColor, flexDirection: 'row', alignSelf: 'flex-end', justifyContent: 'space-around' }}>


            <View style={{ flex: 4, flexDirection: 'row', alignSelf: 'center', alignItems: 'center', padding: 10 }}>
   

            </View>
            <Text style={[styles.MainTitle,{marginRight:10,marginTop:5}]} >
              سعر الدولار                </Text>
            
          </View>

          <View style={{ flex: 2, backgroundColor: '#fff', padding: 10 ,justifyContent:'space-around' }} >
          
            <View style={{ marginTop:10,marginBottom:10,alignItems:'center', flexDirection: 'row', justifyContent: 'flex-end' }}>
              <View style={{ flex: 2, }}>
              <SwitchSelector 
                initial={this.state.is_BuyUSD}
                onPress={value =>  this._is_BuyUSD(value)}
                textColor={c.PrimeryColor} 
                selectedColor={c.SecondColor}
                buttonColor={c.PrimeryColor}
                borderColor={c.SecondColor}
                backgroundColor={c.SecondColor}
                hasPadding
                options={options} 
                bold='true'
                style={{width:'80%'}}
                />
               
              </View>
              <View style={{ flex: 1 }}>
                <Item rounded>
               
                  <Input style={styles.MyInput}   keyboardType="numeric"     onChangeText={BuyUSD => this.setState({ BuyUSD })}  value= {this.state.BuyUSD} />
                </Item>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={[styles.MainTitle1]} >
                  سعر البيع                 </Text>
              </View>
            </View>



            <View style={{ flexDirection: 'row', justifyContent: 'flex-end',marginTop:10,marginBottom:10,alignItems:'center', }}>
              <View style={{ flex: 2, }}>
              <SwitchSelector 
                initial={this.state.is_SellUSD}
                onPress={value =>  this._is_SellUSD(value)}
                textColor={c.PrimeryColor} 
                selectedColor={c.SecondColor}
                buttonColor={c.PrimeryColor}
                borderColor={c.SecondColor}
                backgroundColor={c.SecondColor}
                hasPadding
                options={options} 
                bold='true'
                style={{width:'80%'}}
                />
              </View>
              <View style={{ flex: 1 }}>
                <Item rounded>
                  <Input style={styles.MyInput}    keyboardType="numeric"   onChangeText={SellUSD => this.setState({ SellUSD })}  value= {this.state.SellUSD} />
                </Item>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={[styles.MainTitle1]} >
                  سعر الشراء                 </Text>
              </View>
            </View>


          </View>
        </View>

{/* ////////////////////////////////////////////////////////////////////// */}



<View style={{ margin: 10, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flex: 1, backgroundColor: c.PrimeryColor, flexDirection: 'row', alignSelf: 'flex-end', justifyContent: 'space-around' }}>


            <View style={{ flex: 4, flexDirection: 'row', alignSelf: 'center', alignItems: 'center', padding: 10 }}>


           

            </View>
            <Text style={[styles.MainTitle,{marginRight:10}]} >
              سعر اليورو                 </Text>
          </View>

          <View style={{ flex: 2, backgroundColor: '#fff', padding: 10 ,justifyContent:'space-around' }} >
          
            <View style={{marginTop:10,marginBottom:10,alignItems:'center', flexDirection: 'row', justifyContent: 'flex-end' }}>
              <View style={{ flex: 2, }}>
              <SwitchSelector 
                initial={this.state.is_BuyUo}
                onPress={value =>  this._is_BuyUo(value)}
                textColor={c.PrimeryColor} 
                selectedColor={c.SecondColor}
                buttonColor={c.PrimeryColor}
                borderColor={c.SecondColor}
                backgroundColor={c.SecondColor}
                hasPadding
                options={options} 
                bold='true'
                style={{width:'80%'}}
                />
              </View>
              <View style={{ flex: 1 }}>
                <Item rounded>
                  <Input style={styles.MyInput}    keyboardType="numeric"  onChangeText={BuyUo => this.setState({ BuyUo })}  value= {this.state.BuyUo} />
                </Item>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={[styles.MainTitle1]} >
                  سعر البيع                 </Text>
              </View>
            </View>



            <View style={{ marginTop:10,marginBottom:10,alignItems:'center',flexDirection: 'row', justifyContent: 'flex-end' }}>
              <View style={{ flex: 2, }}>
              <SwitchSelector 
                initial={this.state.is_SellUo}
                onPress={value =>  this._is_SellUo(value)}
                textColor={c.PrimeryColor} 
                selectedColor={c.SecondColor}
                buttonColor={c.PrimeryColor}
                borderColor={c.SecondColor}
                backgroundColor={c.SecondColor}
                hasPadding
                options={options} 
                bold='true'
                style={{width:'80%'}}
                />
              </View>
              <View style={{ flex: 1 }}>
                <Item rounded>
                  <Input style={styles.MyInput}   keyboardType="numeric"    onChangeText={SellUo => this.setState({ SellUo })}  value= {this.state.SellUo} />
                </Item>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={[styles.MainTitle1]} >
                  سعر الشراء                 </Text>
              </View>
            </View>


          </View>
        </View>



   

        <View style={{ flex: 1 ,justifyContent:'flex-end',}}>
        <Text style={{textAlign: "center", alignSelf: "center",}}> اخر تحديث  {this.state.update_time} </Text>
        <TouchableOpacity style={[styles.buttonContainer,{backgroundColor:c.PrimeryColor}]} onPress={()=> this.UpdatePrice()}>
{this.state.SpinnerS1 ? 
  <Spinner color='red' />
   : 
   <Text style={styles.textButton} > تحديث </Text>
}
          
          </TouchableOpacity>


        </View>

       
        </ScrollView>
        :
        <Text style={styles.TheH1} >
        
      حسابك غير مفعل
        </Text>
}
        <KeyboardSpacer/>
      </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
  TheH1: {
    color: '#31a4c1',
    fontSize: 18,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 15,

  },
  MyInput: {
    height: 30,
    textAlign: 'center',
    width: '100%',

    color: colors.PrimeryColor,
  },
  TheMainBox: {
    backgroundColor: colors.SecondColor,
    margin: 20,
    borderRadius: 20,

  },
  MainBox: {
    flexDirection: 'row',
    width: '100%',
    //  height:'20%',
    justifyContent: "center",

  },

  Boxs: {
    width: '40%',
    // height: '10%',
    justifyContent: "center",
    alignSelf: "center",

  },
  MainTitle: {

    color:colors.SecondColor,
    fontSize: 15,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    marginBottom: 5,
    padding: 5,
    justifyContent: 'center',
    
     textAlignVertical: "center",
    color: colors.SecondColor,
    fontSize: 14,
    
    alignItems: 'center' ,

  },
  MainTitle1: {
    width: '100%',
    color: "#696052",
    fontSize: 15,
    // textAlign: "center",
    // alignSelf: "center",
    fontFamily: 'Kan',
    // marginBottom:5,
margin:10


  },
  TextTitle: {
    color: colors.PrimeryColor,
    fontSize: 14,

    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    marginTop: 0,
  },


  TextNumber: {
    color: colors.SecondColor,
    fontSize: 18,
    width: 50,
    height: 50,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 5,
    marginBottom: 15,
    backgroundColor: colors.PrimeryColor,
    borderRadius: 60 / 2,
    borderColor: '#fff',
    paddingTop: 5,
    borderWidth: 2,
  },

  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  TopCon: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "center",
  },
  ButtomCon: {
    flex: 5,
    justifyContent: "center",
  },
  Footer: {
    flex: 2,
    justifyContent: 'flex-end',
    marginBottom: 20,
  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '95%',
    borderRadius: 6,
    height: 40,
    margin: 10,
 

  }, textButton: {
    color:colors.SecondColor,
    fontSize: 12,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
  },

  SmallbuttonContainer: {
    // backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '40%',
    borderRadius: 6,
    marginBottom: 10,

    height: 40,
  },
  inputText: {
    color: colors.PrimeryColor,
    fontSize: 15,
    width: '70%',
    backgroundColor: '#ffffff0C',
    flexDirection: 'row',
    paddingVertical: 5,
    borderRadius: 3,
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    width: '70%',
    marginTop: 10,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    textAlign: "center",
    alignSelf: "center",

  },
  TButton: {
    marginTop: 30,
    height: 40,
    textAlign: "center",
    alignSelf: "center",
    color: colors.PrimeryColor,
  },
  logo: {
    width: '60%',
    marginTop: 150,
    alignSelf: "center",

  }
});
