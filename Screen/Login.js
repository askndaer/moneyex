import { AntDesign } from "@expo/vector-icons";
import { Notifications } from 'expo';
import { Spinner } from "native-base";
import * as Permissions from 'expo-permissions';
import React from "react";
import { Alert, AsyncStorage,ActivityIndicator, Image, ImageBackground, Keyboard, KeyboardAvoidingView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import * as Animatable from "react-native-animatable";
import { NavigationActions } from "react-navigation";
import Config from "./Config";
import { BlurView } from 'expo-blur';
import { Updates } from "expo";
const logo = require("../assets/logo.png");
c = new Config();



export default class login extends React.Component {
  constructor(props) {
    super(props);

    this.navigationOptions = {
      drawerLockMode: "locked-open"
    };

    this.state = {
      isMessage: false,
      UserEmail: "",
      UserPassword: "",
      // UserEmail: "0060183908955",
      // UserPassword: "1234",
      KeyboardOpen: false,
      SpinnerS: false,
      PrimeryColor: c.getPrimeryColor(),
      receivedNotification: null,
      lastNotificationId: null,
      token:null,
      loginBTN:false
    };

    this.props.navigation.addListener("willFocus", x => {
      this.setState({ PrimeryColor: c.getPrimeryColor() });
    });
  }

  performUpdate = async () => {
    try {
      
   
    try {
      const update = await Updates.checkForUpdateAsync();
      if (update.isAvailable) {
        setTimeout(() => this.setState({ isMessage: true }), 100);
        await Updates.fetchUpdateAsync().then(() => {
          setTimeout(() => this.setState({ isMessage: false }), 100);
        });
        Updates.reload();
      } else {
        setTimeout(() => this.setState({ isMessage: false }), 100);
      }
    } catch (e) {
      //alert(e)
      setTimeout(() => this.setState({ isMessage: false }), 100);
    }
    // setTimeout(() => this.setState({ isSplash: false }), 6000);
  } catch (error) {
      // alert(error)
  }
  };
  async  getToken() {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
  
    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
  
    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
      return;
    }
  
    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();
  this.setState({token})
    console.log(token)
  }


  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardWillShow.bind(this)
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardWillHide.bind(this)
    );
  }

  componentDidMount() {
    this.getToken();
  }

  onPressDismissAllNotifications = () => {
    Notifications.dismissAllNotificationsAsync();
    this.setState({
      lastNotificationId: null
    });
  };

  onPressDismissOneNotification = () => {
    Notifications.dismissNotificationAsync(this.state.lastNotificationId);
    this.setState({
      lastNotificationId: null
    });
  };

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardWillShow(e) {
    this.setState({ KeyboardOpen: true });
  }
  _keyboardWillHide(e) {
    this.setState({ KeyboardOpen: false });
  }

  saveKeyEmail_Name_phone = async (
    id,
    Email,
    Name,
    Phone,
    photo,
    is_dollars,
    is_euro,
    type
  ) => {
    AsyncStorage.setItem("@MySuperStore:id", id);
    AsyncStorage.setItem("@MySuperStore:Email", Email);
    AsyncStorage.setItem("@MySuperStore:Name", Name);
    AsyncStorage.setItem("@MySuperStore:Phone", Phone);
    if (photo != null) AsyncStorage.setItem("@MySuperStore:photo", photo);
    AsyncStorage.setItem("@MySuperStore:Type", type);

    AsyncStorage.setItem("@MySuperStore:is_dollars", is_dollars);
    AsyncStorage.setItem("@MySuperStore:is_euro", is_euro);
  };

  async saveLogin() {
    try {
      await AsyncStorage.setItem("userToken", "ture");
      console.log("11111111111111111111111111111111111111111");
    } catch (error) {}
  }

  GuestLogin = async () => {
    const L_isLogin = await AsyncStorage.getItem("Message");

    await AsyncStorage.setItem("@MySuperStore:Type", "false");
    //  console.log("11111111111111111111111111111111111111111");
    //  console.log(L_isLogin);
    //  c.PrimeryColor='#fff';
    this.props.navigation.navigate("Main");
  };

  GuestLogin1 = async () => {
    await AsyncStorage.setItem("@MySuperStore:Type", "false");
    // console.log("11111111111111111111111111111111111111111");
    //  c.PrimeryColor='#fff';
    this.props.navigation.push("Register");
  };

  // async GuestLogin() {
  //   try {
  //  //   await AsyncStorage.setItem('@MySuperStore:Type', "false");
  //     console.log("11111111111111111111111111111111111111111");
  //     this.navigateToScreen('Main')

  //   } catch (error) {
  //     console.log(error);
  //   }
  // }
  navigateToScreen(route, x) {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
      header: null
    });
    this.props.navigation.navigate(x, navigateAction);
  }

  Signup = async () => {
    this.setState({
      SpinnerS: true,loginBTN:true
    });
    fetch("http://konuze.com/index.php/api/example/login", {
      // fetch('http://192.168.0.136/2/application/index.php/api/example/login', {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      // body: "email=ali@ali.com&password=1" // <-- Post parameters
      body:
        "phone_number=" +
        this.state.UserEmail +
        "&password=" +
        this.state.UserPassword // <-- Post parameters
    })
      .then(response => response.json())

      .then(responseJson => {
        this.setState({
          SpinnerS: false,loginBTN:false
        });

        console.log(responseJson);

        if (responseJson["response"] == 1) {
          let MyResponse = responseJson["user"][0];
          if (MyResponse["type"] == "client") {
            this.saveLogin();
            this.saveKeyEmail_Name_phone(
              MyResponse["id"],
              MyResponse["email"],
              MyResponse["name"],
              MyResponse["phone_number"],
              MyResponse["image"],
              MyResponse["is_dollars"],
              MyResponse["is_euro"],
              "client"
            );
            
            if(MyResponse["status"]==0){
              Alert.alert("تنبية", "تم حظر الحساب");
              return 
            }
            this.navigateToScreen("Main", "LoginN");
          } else {
            this.saveLogin();
            this.saveKeyEmail_Name_phone(
              MyResponse["id"],
              MyResponse["email"],
              MyResponse["name"],
              MyResponse["phone_number"],
              MyResponse["image"],
              MyResponse["is_dollars"],
              MyResponse["is_euro"],
              "company"
            );
            this.navigateToScreen("MainCompany", "LoginNCompany");
          }
        } else {
          Alert.alert("تنبية", responseJson["message"]);
        }
      })
      .catch(error => {
        Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
        this.setState({
          SpinnerS: false,loginBTN:false
        });
      });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <KeyboardAvoidingView style={styles.container} behavior="padding">
          <ImageBackground
            style={styles.imgBackground}
            resizeMode="cover"
            source={require("../assets/main.png")}
          >

          <Text style={{position:'absolute',top:40,left:10,fontSize:10}}>V 1.0</Text>
            {!this.state.KeyboardOpen ? (
              <Animatable.View
                animation="slideInDown"
                iterationCount={1}
                direction="alternate"
                style={styles.TopCon}
              >
                <Image source={logo} style={styles.logo} resizeMode="contain" />
              </Animatable.View>
            ) : (
              <Animatable.View
                animation="slideInDown"
                iterationCount={1}
                direction="alternate"
                style={styles.TopCon}
              />
            )}

            <TouchableOpacity
              style={{ top: 40, right: 20, position: "absolute" }}
              onPress={() => this.props.navigation.navigate("colorpicker")}
            >
              <AntDesign
                name="setting"
                size={20}
                color={this.state.PrimeryColor}
              />
            </TouchableOpacity>
            <Animatable.View
              animation="fadeInUp"
              iterationCount={1}
              direction="alternate"
              style={styles.ButtomCon}
            >
              <TouchableOpacity
                style={[
                  styles.buttonContainer,
                  { backgroundColor: this.state.PrimeryColor }
                ]}
                onPress={this.GuestLogin}
              >
                <Text style={styles.textButton}> دخول كـ ضيف</Text>
              </TouchableOpacity>

              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <View
                  style={{
                    flexDirection: "row",
                    width: "70%",
                    marginTop: 15,
                    marginBottom: 15
                  }}
                >
                  <View
                    style={{
                      backgroundColor: c.PrimeryColor,
                      height: 1,
                      flex: 1,
                      alignSelf: "center"
                    }}
                  />
                  <Text
                    style={{
                      color: c.PrimeryColor,
                      fontFamily: "Kan",
                      fontSize: 15,
                      alignSelf: "center",
                      paddingHorizontal: 5
                    }}
                  >
                    {" "}
                    أو{" "}
                  </Text>
                  <View
                    style={{
                      backgroundColor: c.PrimeryColor,
                      height: 1,
                      flex: 1,
                      alignSelf: "center"
                    }}
                  />
                </View>
              </View>

              <TextInput
                style={styles.inputText}
                placeholder="رقم الهاتف"
                placeholderTextColor={this.state.PrimeryColor}
                onChangeText={UserEmail => this.setState({ UserEmail })}
              />
              <TextInput
                style={styles.inputText}
                secureTextEntry={true}
                placeholder="كلمة السر"
                placeholderTextColor={this.state.PrimeryColor}
                onChangeText={UserPassword => this.setState({ UserPassword })}
              />

              <TouchableOpacity
                style={[
                  styles.buttonContainer,
                  { backgroundColor: this.state.PrimeryColor }
                ]}
                onPress={this.Signup}
              >
                {this.state.SpinnerS ? (
                  <Spinner color="red" />
                ) : (
                  <Text style={styles.textButton}> دخول</Text>
                )}
              </TouchableOpacity>

              <TouchableOpacity onPress={this.GuestLogin1} disabled={this.state.loginBTN}>
                <Text
                  style={[styles.TButton, { color: this.state.PrimeryColor }]}
                >
                  {" "}
                  انشاء مستخدم جديد
                </Text>
              </TouchableOpacity>
            </Animatable.View>
            {this.state.isMessage ? (
          <BlurView
            tint="dark"
            intensity={77}
            style={{
              position: "absolute",
              height: "100%",
              width: "100%",
              justifyContent: "center"
            }}
          >
            <View
              style={[
                {
                  borderRadius: 20,
                  width: "80%",
                  backgroundColor: "#fff",
                  padding: 20,
                  alignSelf: "center"
                },
               
              ]}
            >
              <Text style={{ fontSize: 16,}}>INFO</Text>
              <View
                style={{
                  width: "100%",
                  height: 1,
                  backgroundColor: "#7f8c8d",
                  marginTop: 5
                }}
              />
              {/* <MaterialIcons name="fingerprint" size={50} color={colors.PrimaryColor} style={{ alignSelf: 'center', marginTop: 10 }} /> */}

              <Text style={{ marginTop: 20 }}>
                Updating your application, please wait..
              </Text>
              <ActivityIndicator
                size="small"
                color={'#9c1116'}
                style={{ margin: 10 }}
              />
            </View>
          </BlurView>
        ) : null}
          </ImageBackground>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    alignItems: "center",
    justifyContent: "center"
  },
  imgBackground: {
    width: "100%",
    height: "100%",
    flex: 1
  },
  TopCon: {
    flex: 1
  },
  ButtomCon: {
    flexDirection: "column",
    flex: 2,
    justifyContent: "flex-end"
  },
  buttonContainer: {
    // backgroundColor: c.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: "70%",
    borderRadius: 6,
    marginTop: 20,
    height: 40
  },
  textButton: {
    color: c.SecondColor,
    fontSize: 15,
    fontFamily: "Kan",
    textAlign: "center",
    alignSelf: "center"
  },
  inputText: {
    color: c.PrimeryColor,
    fontSize: 15,
    width: "70%",
    backgroundColor: "#ffffff0C",
    flexDirection: "row",
    paddingVertical: 5,
    borderRadius: 3,
    borderColor: c.PrimeryColor,
    borderWidth: 1,
    width: "70%",
    marginTop: 10,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    textAlign: "center",
    alignSelf: "center"
  },
  TButton: {
    fontFamily: "Kan",
    marginTop: 30,
    height: 40,
    textAlign: "center",
    alignSelf: "center",
    color: c.PrimeryColor
  },

  logo: {
    width: "80%",
    // backgroundColor:'#000',
    height: 150,
    marginTop: 150,
    alignSelf: "center"
  }
});
