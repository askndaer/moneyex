import React from 'react';
import { StyleSheet, Modal,Text, Alert, View, AsyncStorage,ImageBackground, ScrollView, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { AppRegistry, FlatList,  ActivityIndicator, Platform} from 'react-native';
import { Drawer, Thumbnail, Icon } from 'native-base';
import SideBar from './MySiderBar';
import colors from './Color';
import Header from './Header';
import MySiderBar from './MySiderBar';
import { Card } from 'react-native-elements';


export default class login extends React.Component {
  constructor(props)
  {
     super(props);
     this.state = { 
    isLoading: true
  }

  this.props.navigation.addListener('willFocus', (x) => {

    const { navigation } = this.props;
    const Type = navigation.getParam('Type', 'US');
  
  this.fetchData(Type);
})

  }
  opeToast = () => {
    // alert('fsd'),
    Alert.alert(
      'نمبية',
      'لا يمكنك مشاهدة المحتوى ',
      [
        {text: 'تسجيل الدخول ', onPress: () => console.log('Ask me later pressed')},
        {text: 'مستخدم جديد', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        
      ],
      { cancelable: false }
    )
    
    };
    fetchData = async(Type) =>{
      const id1 = await AsyncStorage.getItem('@MySuperStore:id');
    fetch('http://konuze.com/index.php/api/example/get_all_complaintsByid', {

      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
      }),
      body: "id="+id1

    }).then((response) => response.json())

    .then((responseJson) => {
    
      console.log("11111111111111111111111111111111111111111");
      
      let MyResponse = responseJson['users'];
  //    console.log(MyResponse);
      // if (responseJson.response==0){
  //      console.log(MyResponse);
            this.setState({
              isLoading: false,
              dataSource: MyResponse
            }, function () {
              // In this block you can do something with new state.
            });
// }
}).catch((error) => {
  Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
  this.setState({
    SpinnerS: false
  })
});

}

componentWillReceiveProps() {
  console.log('rerender here')
  //this.yourFunction()
  //this.setState({})
}
  render() {

    CustomProgressBar = ({ visible }) => (
      <Modal onRequestClose={() => null} visible={visible}>
        <View style={{ flex: 1, backgroundColor: '#dcdcdc', alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ borderRadius: 10, backgroundColor: 'white', padding: 25 }}>
            <Text style={{ fontSize: 14, fontWeight: '200', fontFamily: 'Kan', }}>جلب البيانات</Text>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <CustomProgressBar />
        </View>
      );
    }
    

    return (


      <ImageBackground style={styles.imgBackground} ref="view1"
        resizeMode='cover'
        source={require('../assets/main.png')}>

        <Header  navigation={this.props.navigation}  />


          
            <Animatable.View animation="slideInDown" iterationCount={1} direction="alternate" style={styles.TopCon}>
              <Text style={[styles.TheH1,{color:c.PrimeryColor}]} > قامة الشكاوى
              </Text>
            </Animatable.View>
        
          <View style={styles.ButtomCon}>

          <FlatList
       
       data={ this.state.dataSource }
       
       ItemSeparatorComponent = {this.FlatListItemSeparator}

       renderItem={({item}) =>
            <ScrollView>

              <TouchableOpacity onPress={() => {
            this.props.navigation.navigate('ViewComp', {
              complaint_title: item.complaint_title,
              id: item.id,
              created_at:item.created_at,
              status:item.status,
              company_name:item.company_name
            });
          }} >
                <Card >
                  <View style={styles.MainBox}>
                    <Text style={[styles.TextNumber, { flex: 1, alignSelf: 'center' }]} >
                      <Icon name="md-arrow-dropleft" style={{ fontSize: 25, color: c.PrimeryColor }} />
                    </Text>
                    <View style={{ flex: 7, }}>
                    <Text style={[styles.TextTitle1,{color:c.PrimeryColor}]} >{ item.complaint_title} </Text>
                      <Text style={[styles.TextTitle1,{color:c.PrimeryColor}]} >{ item.company_name} </Text>
                      <Text style={[styles.TextTitle1,{color:c.PrimeryColor}]} >{ item.created_at} </Text>
                      <Text style={[styles.TextTitle1,{color:c.PrimeryColor}]} >{ item.status} </Text>
                    </View>
                    <View style={{ flex: 2, }}>
                    <Text style={[styles.TextTitle,{color:c.PrimeryColor}]} >العنوان</Text>
                      <Text style={[styles.TextTitle,{color:c.PrimeryColor}]} >الوكالة </Text>
                      <Text style={[styles.TextTitle,{color:c.PrimeryColor}]} >التاريخ </Text>
                      <Text style={[styles.TextTitle,{color:c.PrimeryColor}]} >الحالة </Text>
                    </View>
                  </View>
                </Card>
              </TouchableOpacity>
  

              </ScrollView>
}
keyExtractor={(item, index) => index.toString()}
/>


{/* <View style={styles.Footer}> */}

          {/* <TouchableOpacity style={styles.buttonContainer} onPress={() => this.props.navigation.navigate('NewComp')}>
            <Text style={styles.textButton} > اضافة شكوى جديدة</Text>
          </TouchableOpacity> */}

{/* </View> */}

          </View>


        </ImageBackground>
     
    );
  }
}

const styles = StyleSheet.create({
  TheH1: {
   
    fontSize: 20,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    // margin: 10,

  },
  TheMainBox: {
    // backgroundColor: colors.SecondColor,
    backgroundColor: '#f2f3f3',
    marginTop: 5,
    borderRadius: 10,
    marginLeft: 20,
    marginRight: 20,
    // borderWidth:2,
  },
  MainBox: {
    // backgroundColor:'#colors.SecondColor,',
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    justifyContent: "flex-end",
    // height:140,
    padding: 10,

  },

  Boxs: {
    width: '40%',
    // height: '10%',
    justifyContent: "center",
    alignSelf: "center",

  },
  MainTitle: {
    backgroundColor: '#fff',
    color: colors.SecondColor,
    fontSize: 20,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    marginBottom: 5,
    padding: 10,
    marginTop: -30,
    borderRadius: 20,
    borderColor: colors.SecondColor,

    borderWidth: 2,
  },
  TextTitle: {
   
    

    fontSize: 14,
    alignSelf: 'flex-end',
    fontFamily: 'Kan',
  },
  TextTitle1: {
  
    fontSize: 14,
    alignSelf: "flex-end",
    fontFamily: 'Kan',
  },

  TextNumber: {
   
    fontSize: 24,
    fontFamily: 'Kan',

  },

  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  TopCon: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "center",
  },
  ButtomCon: {
    flex: 10,

  },
  Footer: {
    flex: 1,

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '90%',
    borderRadius: 6,
    marginTop: 10,
    height: 40,

  }, textButton: {
    color: '#333133',
    fontSize: 15,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
  },

  SmallbuttonContainer: {
    // backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '40%',
    borderRadius: 6,
    marginBottom: 10,

    height: 40,
  },
  inputText: {
    color: colors.PrimeryColor,
    fontSize: 15,
    width: '70%',
    backgroundColor: '#ffffff0C',
    flexDirection: 'row',
    paddingVertical: 5,
    borderRadius: 3,
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    width: '70%',
    marginTop: 10,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    textAlign: "center",
    alignSelf: "center",

  },
  TButton: {
    marginTop: 30,
    height: 40,
    textAlign: "center",
    alignSelf: "center",
    color: colors.PrimeryColor,
  },
  logo: {
    width: '60%',
    marginTop: 150,
    alignSelf: "center",

  }
});
