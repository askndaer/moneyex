import React from 'react';
import { StyleSheet,SegmentedControlIOS, Alert,Text,FlatList,ScrollView, View, AsyncStorage,ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container,  Content, Textarea,Form, Item, Input, Label, Right } from 'native-base';
import colors from './Color';
const logo = require("../assets/logo.png");
import SwitchSelector from 'react-native-switch-selector';
import { Divider } from 'react-native-elements';
import { Drawer,Picker } from 'native-base';
import Header from './Header';
import MySiderBar from './MySiderBar';
import SideBar from './MySiderBar';
import {  Spinner } from 'native-base';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import HideWithKeyboard from 'react-native-hide-with-keyboard';
let uri = "http://style.anu.edu.au/_anu/4/images/placeholders/person.png"
export default class user_register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "key1",
      complaint_title:'لا  عنوان',
      comment:'',
      title:'no',
      photo:'',
      refresh:true,
      client_id:null,

    };
    this.flatlistRef = React.createRef();
  
  
    this.props.navigation.addListener('willFocus', (x) => {

      this.getKey();
      const { navigation } = this.props;
      const id = navigation.getParam('id', '0');
      this.GetDataFromServer(id);

  
  })
    
  }

  async getKey() {
    try {
        const id = await AsyncStorage.getItem('@MySuperStore:id');
        const photo = await AsyncStorage.getItem('@MySuperStore:photo');
        const L_Type = await AsyncStorage.getItem('@MySuperStore:Type');
        this.setState({ photo: photo });
        this.setState({ client_id: id });
        if(L_Type != 'company')
        uri="http://konuze.com/assets/images//client/"+photo;
        else
        uri="http://konuze.com/assets/images/"+photo;

        } catch (error) {
            console.log("Error retrieving data" + error);
        }
}




GetDataFromServer(x){
  const { client_id } = this.state;
  fetch('http://konuze.com/index.php/api/example/get_replay_complaint_info_by_id', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
    }),
    body: "id="+x // <-- Post parameters
  }).then((response) => response.json())
  .then((responseJson) => {
    console.log("11111111111111111111111111111111111111111");
    let MyResponse = responseJson['replays'];
          this.setState({
            isLoading: false,
            dataSource: MyResponse
          }, function () {
          });
}).catch((error) => {
Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
this.setState({
  SpinnerS: false
})
});


}

Signup = async () => {

  
  const { title } = this.state;
  const { comment } = this.state;
  
  const { client_id } = this.state;
  const { navigation } = this.props;
    const id = navigation.getParam('id', '0');
  console.log(" cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc "+comment);
console.log(" vvvvvvvvvvvvvv "+comment);
  if (comment  != ""){  
    // 'title' => $_POST['title'],
    // 'replay' => $_POST['replay'],
    // 'complaint_id' =>  $_POST['complaint_id'],
    // 'by' => 'user',
    // 'created_at' => current_datetime()
  let Mybody="title="+ title+"&"+
              "replay="+ comment+"&"+
              "byid="+ client_id+"&"+
              "complaint_id="+ id+"";
     
              console.log(" vvvvvvvvvvvvvv "+Mybody);
  this.setState({
    SpinnerS: true
  })
 
  fetch('http://konuze.com/index.php/api/example/addreply', {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
      }),
      body: Mybody
    }).then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        SpinnerS: false
      })
    
      console.log(" vvvvvvvvvvvvvv111 "+responseJson['response']);
      this.GetDataFromServer(id);
 
      if (responseJson['response']==1){
      
    
        this.setState({
          comment: ''
        })
      
      //  Alert.alert("تنبية", responseJson.message);

      }else{
       Alert.alert("تنبية", responseJson.message);
      }
     }).catch((error) => {
     // Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
      this.setState({
        SpinnerS: false
      })
    });
  }else{
    // console.log("333333333333333333333333"  );
    Alert.alert("تنبية", 'يجب كتابة الرد اولاً');
  }
}




  render() {

    const { navigation } = this.props;
    const complaint_title = navigation.getParam('complaint_title', '0');
    const id = navigation.getParam('id', '0');
    const created_at = navigation.getParam('created_at', '0');
    const status = navigation.getParam('status', '0');
    const company_name = navigation.getParam('company_name', '0');
    //
    return (

    

      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/main.png')}>


<Header  navigation={this.props.navigation}  />

          


<HideWithKeyboard>
          <View>        
        <Text style={styles.TheH1}>
           {complaint_title}
            </Text>

         
          <Animatable.View ref="view1"  animation="fadeInDown" iterationCount={1} direction="alternate" >
           

          <View style={styles.MainBox}>
                
                    <View style={{ flex: 7, }}>
                      <Text style={[styles.TextTitle1]} >{company_name} </Text>
                      <Text style={[styles.TextTitle1]} >{created_at} </Text>
                      <Text style={[styles.TextTitle1]} >{status} </Text>
                    </View>
                    <View style={{ flex: 2, }}>
                      <Text style={[styles.TextTitle]} >الوكالة </Text>
                      <Text style={[styles.TextTitle]} >التاريخ </Text>
                      <Text style={[styles.TextTitle]} >الحالة </Text>
                    </View>
                  </View>

            </Animatable.View>
         
          <Divider style={{ backgroundColor: colors.PrimeryColor,margin:20}} />

          </View>
          </HideWithKeyboard>

        <ScrollView   
         ref={ref => this.scrollView = ref}
         onContentSizeChange={(contentWidth, contentHeight)=>{        
             this.scrollView.scrollToEnd({animated: true});
         }}
        
        >
        
          <FlatList
          ref={this.flatlistRef}
        

      extraData={ this.state.refresh }
       data={ this.state.dataSource }
       ItemSeparatorComponent = {this.FlatListItemSeparator}
       renderItem={({item}) =>

       <TouchableOpacity 
          style={{ flex: 1,
            justifyContent: 'flex-end',
            marginBottom: 5}}
          >


{item.by == 'admin'?

<View style={{flexDirection:'row'}}>
<Image    source={require('../assets/admin.png')}
style={{ margin: 10, width: 45, height: 45, borderRadius: 45/2}} />


<View style={{backgroundColor:'#f1f2f6',margin:2,padding:15,marginRight:100,  borderRadius: 10, borderColor:'#ebedef',
    borderWidth: 1,}}>



      <Text style={[styles.TextTitle]} >{item.replay} </Text>
      </View>
      </View>
:
<View style={{flexDirection:'row',justifyContent:'flex-end'}}>


<View style={{backgroundColor:'#f1f2f6',margin:2,padding:15,marginLeft:100,  borderRadius: 10, borderColor:'#ebedef',
    borderWidth: 1,}}>
      <Text style={[styles.TextTitle,{textAlign:'left'}]} >{item.replay} </Text>
      </View>
      <Image source={{ uri: uri }} 
style={{ margin: 10, width: 45, height: 45, borderRadius: 45/2}} />
      </View>
        }
</TouchableOpacity>
  


}
keyExtractor={(item, index) => index.toString()}
/>




</ScrollView>

        

        <View style={{ flexDirection: 'row', margin: 10, alignSelf: 'flex-end' }}>
          <View style={{ flex: 4 }}>
            <Item stackedLabel >

              <Input value={this.state.comment} style={[styles.MyInput, { textAlign: "center" }]} onChangeText={comment => this.setState({ comment })} />
            </Item>
          </View>
          <View style={{ flex: 1 }}>
            <TouchableOpacity style={[styles.buttonContainer,{backgroundColor:c.PrimeryColor}]} onPress={() => this.Signup()}>
            {this.state.SpinnerS ? 
          <Spinner color='red' />
           : 
           <Text style={[styles.textButton,]} > رد </Text>
        }
   
            </TouchableOpacity>
          </View>
          
        </View>
        <KeyboardSpacer/>
      </ImageBackground>
 
    );
  }
}

const styles = StyleSheet.create({

  imgBackground: {
    width: '100%',
    height: '100%',
flex:1,
  },
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 18,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 5,
   
  },
  Label1: {

    color: colors.SecondColor,
    textAlign: "right",
    fontFamily: 'Kan',
    width: '100%',
    marginTop: 20,
  },
  MyInput: {
    height: 30,
    textAlign: 'right',
    width: '100%',
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    color: colors.PrimeryColor,
  },
  Myfrom: {
    borderRadius: 20,
    padding: 20,

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 20,
    height: 40,

  }
  , textButton: {
    color: '#333133',
    fontSize: 15,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
  MainBox: {
    // backgroundColor:'#colors.SecondColor,',
    flexDirection: 'row',

    width: '100%',
    justifyContent: "flex-end",
    // height:140,
    padding: 10,

  },TextTitle: {
    color: "#000",
    

    fontSize: 14,
    alignSelf: 'flex-end',
    fontFamily: 'Kan',
  },
  TextTitle1: {
    color: '#000',
    fontSize: 14,
    alignSelf: "flex-end",
    fontFamily: 'Kan',
  },

});
