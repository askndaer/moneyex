import React from 'react';
import { AsyncStorage } from 'react-native';

import MainPageScreen from './Register';
import MainPageScreen1 from './user_register1';
import SideMenu from './MySiderBar';
import LoginScreen from './Login';
import MainScreen from './Main';
import Register from './Register';
import TestScreen from './test.2';
import PricelistScreen from './pricelist';
import passwordChangeScreen from './passwordChange';

import NewCompScreen from './NewComp';
import comListScreen from './comList';

import NewMesScreen from './NewMes';
import mesListScreen from './mesList';
import ViewMesScreen from './ViewMes';

import ViewCompScreen from './ViewComp';
import AuthLoadingScreen from './AuthLoading.screen';
import aboutusScreen from './aboutus';

import uploScreen from './uplo';
import colorpickerScreen from './colorpicker';
import agintviewScreen from './agintview'


import RegisterCompany from './registerCompany';
import RegisterUser from './RegisterUser'

import MainScreenCompany from './Main_company';
import { DrawerNavigator, createSwitchNavigator, createDrawerNavigator } from 'react-navigation';
import {
  
  createAppContainer
} from 'react-navigation';


import { createStackNavigator } from 'react-navigation-stack';


const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 750,
      // easing: Easing.out(Easing.poly(4)),
      easing: Easing.back(1),
      // easing: Easing.
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps

      const thisSceneIndex = scene.index
      const width = layout.initWidth

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0],
      })

      return { transform: [{ translateX }] }
    },
  }
}




const LoginNavigator = createDrawerNavigator({
  
 
  // TestScreen: { screen: TestScreen, navigationOptions: { header: null }, },
  Main: { screen: MainScreen, navigationOptions: { header: null } },
  
  passwordChange: { screen: passwordChangeScreen, navigationOptions: { header: null }, },
  aboutus: { screen: aboutusScreen, navigationOptions: { header: null }, },

  uplo: { screen: uploScreen, navigationOptions: { header: null }, },
  Pricelist: { screen: PricelistScreen, navigationOptions: { header: null }, },
  agintview: { screen: agintviewScreen, navigationOptions: { header: null }, },
 
  NewComp: { screen: NewCompScreen, navigationOptions: { header: null }, },
  comList: { screen: comListScreen, navigationOptions: { header: null }, },
  
  NewMes: { screen: NewMesScreen, navigationOptions: { header: null }, },
  mesList: { screen: mesListScreen, navigationOptions: { header: null }, },
  ViewMes: { screen: ViewMesScreen, navigationOptions: { header: null }, },
  
  ViewComp: { screen: ViewCompScreen, navigationOptions: { header: null }, },
  ViewComp: { screen: ViewCompScreen, navigationOptions: { header: null }, },
  passwordChange: { screen: passwordChangeScreen, navigationOptions: { header: null }, },
}, {
    contentComponent: SideMenu,
    drawerPosition: 'right',
    drawerWidth: 300,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',

    drawerType: 'back',
  },

);

const LoginNavigatorCompany = createDrawerNavigator({
  MainCompany: { screen: MainScreenCompany, navigationOptions: { header: null } },
  Main: { screen: MainScreen, navigationOptions: { header: null } },
  Pricelist: { screen: PricelistScreen, navigationOptions: { header: null }, },
  passwordChange: { screen: passwordChangeScreen, navigationOptions: { header: null }, },
  aboutus: { screen: aboutusScreen, navigationOptions: { header: null }, },
  uplo: { screen: uploScreen, navigationOptions: { header: null }, },
}, {
    contentComponent: SideMenu,
    drawerPosition: 'right',
    drawerWidth: 300,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',

    drawerType: 'back',
  },

);

const LoginStack = createStackNavigator({
  Login: { screen: LoginScreen, navigationOptions: { drawerLockMode: 'locked-open', header: null, }, },
  Register: { screen:Register, navigationOptions: {header: null}, },
  RegisterUser: { screen: RegisterUser, navigationOptions: { header: null } },
  RegisterCompany: { screen: RegisterCompany, navigationOptions: { header: null } },

});
createStackNavigator


const LogOutNavigator = createDrawerNavigator({
  // 
 
  // Main: { screen: MainScreen, navigationOptions:  { drawerLockMode: 'locked-open', header: 'das', }, },
  Login: {
    screen: LoginStack,
  },
 
  Main: { screen: MainScreen, navigationOptions:  { drawerLockMode: 'locked-open', header: 'das', }, },
  Pricelist: { screen: PricelistScreen, navigationOptions: { header: null }, },
  
  passwordChange: { screen:passwordChangeScreen, navigationOptions: {header: null}, },
  
  colorpicker: { screen: colorpickerScreen,navigationOptions: {header: null}, },

},
{
  drawerLockMode: 'locked-closed'
}, {
  contentComponent: SideMenu,
  drawerPosition: 'right',
  drawerWidth: 300,
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle',
 
  drawerType: 'back',
  contentOptions: {
    activeTintColor: '#e91e63',
    itemsContainerStyle: {
        marginVertical: 0,
    },
    iconContainerStyle: {
        opacity: 1
    }
  }
},

);















const AppNavigator = createSwitchNavigator(

  {
    
    AuthLoading: {screen: AuthLoadingScreen},
    LogOutN: {screen: LogOutNavigator},
    LoginN: {screen: LoginNavigator},
    LoginNCompany: {screen: LoginNavigatorCompany},
    
  },
  {
    initialRouteName: 'AuthLoading',
  }
);




const App = createAppContainer(AppNavigator);
export default App



