import React from 'react';
import { StyleSheet,WebView, Text, Platform,View, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container,  Content, Textarea,Form, Item, Input, Label, Right } from 'native-base';
import colors from './Color';
const logo = require("../assets/logo.png");
import SwitchSelector from 'react-native-switch-selector';
import { CheckBox } from 'react-native-elements';
import { Drawer,Picker } from 'native-base';
import Header from './Header';
import MySiderBar from './MySiderBar';
import SideBar from './MySiderBar';
import { ScrollView } from 'react-native-gesture-handler';



export default class user_register extends React.Component {
  

  render() {

    

    return (

     


      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/main.png')}>

<Header  navigation={this.props.navigation} />
{/* 
<WebView
  
  // {Platform.OS === 'ios' ? source={PolicyHTML} : source={PolicyHTML}}
  source={Platform.OS === 'ios' ? require('../Htmlfiles/aboutus.html') : {uri: "file:///android_asset/Htmlfiles/aboutus.html"}}
  style={{flex: 1,backgroundColor:'#ffffff00'}}
 /> */}
<ScrollView>
<Text style={styles.TheH1}>
شركة الغيث للعملات النقدية
</Text>

<Text style={styles.TheH2}>


تعرف تجارة العملات أو تداول العملات الأجنبية باسم الفوركس، وهو عبارة عن سوق عالمية لا مركزية يتم فيها تداول وتبادل العملات في مختلف أنحاء العالم وفقاً لأسعار صرف العملات، ويعدّ سوق الفوركس من أكبر وأكثر الأسواق سيولة حيث يتجاوز معدل حجم تبادل العملات اليومي فيه 5 ترليون دولار، وهو معدل كبير جداً؛ حيث إنّ جميع الأسهم العالمية مجتمعة لا تقترب من حجم التداول هذا،[١] ويسهّل سوق الفوركس عملية بيع وشراء العملات في مختلف أنحاء العالم، وتكمن آلية العمل في تحقيق الربح عن طريق بيع العملة في حال ارتفاع سعرها، وشراء العملات المنخفضة، والجدير بالذكر أنّه توجد درجة عالية من المخاطر الناتجة عن صفقات التبادل والتي قد تؤدّي إلى حدوث خسائر كبيرة لتُجّار العملة.[٢] وتحتلّ العملة أهمية كبيرة في حياة الأفراد في مختلف أنحاء العالم؛ حيث يحتاج الفرد لتبادلها والتحويل من عملة إلى أخرى من أجل تسيير الأعمال التجارية المختلفة، وعلى سبيل المثال، إذا أراد سائح فرنسي أن يشتري سلعة ما من مصر، أو أن يدفع لرؤية الأهرامات في مصر، فلن تُقبَل العملة الفرنسية هناك، إذ يجب عليه تحويل اليورو إلى الجنيه المصري، وهي العملة المقبولة في مصر، وبالتالي فإنه سوف يتبادل العملة وفقاً لسعر الجنيه المصري في ذلك الوقت، ويمكن قياس ذلك على مختلف المواقف والأعمال التجارية المختلفة، وعلى هذا النحو يظهر السبب الرئيسي في كون سوق تبادل العملات من أكبر الأسواق المالية وأكثرها سيولة في العالم، فحاجة الأفراد إلى تبادل العملات هو ما يجعله على قدر من الأهمية
</Text>
</ScrollView>

      </ImageBackground>
   
    );
  }
}

const styles = StyleSheet.create({

  imgBackground: {
    width: '100%',
    height: '100%',

  },
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 30,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 25,
    
  },
  TheH2: {
    color:"#000",
    fontSize: 18,
    // textAlign: "center",
    // alignSelf: "center",
    fontFamily: 'Kan',
    margin: 25,
    lineHeight:35,
    writingDirection:'rtl',
    // textAlign:'justify',
   

    
  },
  Label1: {

    color: colors.SecondColor,
    textAlign: "right",
    fontFamily: 'Kan',
    width: '100%',
    marginTop: 20,
  },
  MyInput: {
    height: 30,
    textAlign: 'right',
    width: '100%',
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    color: colors.PrimeryColor,
  },
  Myfrom: {
    borderRadius: 20,
    padding: 20,

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 20,
    height: 40,

  }
  , textButton: {
    color: '#333133',
    fontSize: 15,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
});
