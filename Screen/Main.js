import React from 'react';
import { StyleSheet,BackHandler, Text, View,Alert, ImageBackground, TouchableOpacity, Image, TextInput,AsyncStorage } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Drawer,Thumbnail } from 'native-base';
import MySiderBar from './MySiderBar';
import colors from './Color';
import Header from './Header';
import { DrawerNavigator } from "react-navigation";
import {  Spinner } from 'native-base';
import Moment from 'moment';
import NumberFormat from "react-number-format";
import axios from "axios";
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
export default class Main extends React.Component {

  constructor (props) {
    super(props);
    
    this.state = {
      BuyUSD: '0.0',
      SellUSD: '0.0',
      BuyUo:'0.0',
      SellUo:'0.0',
      update_time:'',
      SpinnerS:false,
      PrimeryColor:c.PrimeryColor
    };
    

    this.props.navigation.addListener('willFocus', (x) => {

      const { navigation } = this.props;
      const id = navigation.getParam('id', '0');
      this.GetDataFromServer();
      this.setState({PrimeryColor: c.PrimeryColor});
     
  })
}


componentDidMount(){
  this.updateToken();
}
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backPressed);
   
 }
 
 componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
 }



 async updateToken(){
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
    return;
  }
  const id =    await AsyncStorage.getItem('@MySuperStore:id');

  // Get the token that uniquely identifies this device
  let token = await Notifications.getExpoPushTokenAsync();
  const instance = axios.create()
  instance.defaults.timeout = 2500;
  instance.post("http://konuze.com/index.php/api/example/token_update_client","id="+id+"&token="+token)
  .then(responseJson => {
      console.log(responseJson.data)
  });
}

 GetDataFromServer = async () => {

  this.setState({
    SpinnerS: true
  })

  const instance = axios.create()
  instance.defaults.timeout = 2500;
  instance.post("http://konuze.com/index.php/api/example/moneyrate")
  .then(responseJson => {

      this.setState({
        SpinnerS: false
      })
      console.log("11111111111111111111111111111111111111111");
      console.log(responseJson.data);
      let MyResponse = responseJson.data[0];
      if (MyResponse.response==1){
        this.setState({BuyUSD: MyResponse.a1});
          this.setState({SellUSD:  MyResponse.a2});
          this.setState({BuyUo:  MyResponse.a3});
          this.setState({SellUo:  MyResponse.a4});

          var dt =MyResponse.update_time;
         
          this.setState({update_time:  Moment(dt).format('YYYY-MM-DD     hh:mm') });
          
      }else{
        Alert.alert("تنبية", MyResponse.message);
      }
     }).catch((error) => {
      Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
      this.setState({
        SpinnerS: false
      })
    });


}

  ListPrice = (x) => {
    this.props.navigation.navigate('Pricelist',
    {
      FromScreen: "Main",
      Type: x,
    }
    );
  }

  backPressed = () => {
    // Alert.alert(
    //   'Exit App',
    //   'Do you want to exit?',
    //   [
    //     {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
    //     {text: 'Yes', onPress: () => BackHandler.exitApp()},
    //   ],
    //   { cancelable: false });
    //   return true;
  }
  
  render() {
   
      closeDrawer = () => {
        this.drawer._root.close()
      };
      openDrawer = () => {
        this.drawer._root.open()
      };



const greeting = 'Welcome to React';
      return (
      
 

  
      <ImageBackground style={styles.imgBackground} ref="view1"
        resizeMode='cover'
        source={require('../assets/main.png')}>

<Header  navigation={this.props.navigation}  />

        <Animatable.View animation="slideInDown" iterationCount={1} direction="alternate" style={styles.TopCon}>
       
          <Text style={[styles.TheH1,{color:c.PrimeryColor}]} >
            أسعار العملات
       </Text>

        </Animatable.View>

        <View style={styles.ButtomCon}>

        <View style={[styles.TheMainBox,{borderColor:c.PrimeryColor,}]}>
            <Text style={[styles.MainTitle,{color:c.PrimeryColor,borderColor:c.PrimeryColor,}]} > سعر الدولار </Text>
            <Animatable.View animation="slideInDown" iterationCount={1} direction="alternate" >
              <View style={styles.MainBox}>
                <TouchableOpacity style={styles.Boxs} onPress={this.ListPrice.bind(this,'US')}>
                  <Text style={[styles.TextTitle,{color:c.PrimeryColor}]} > متوسط الشراء </Text>

                  <NumberFormat
                      value={this.state.SellUSD}
                      displayType={"text"}
                      thousandSeparator={true}
                       decimalScale={4}
                      fixedDecimalScale={true}
                      renderText={value => (
                        <Text style={[styles.TextNumber,{backgroundColor:c.PrimeryColor,}]} >{value}</Text>
                      )}
                    />
                </TouchableOpacity>


                <TouchableOpacity style={styles.Boxs}  onPress={this.ListPrice.bind(this,'US')}>
                  <Text style={[styles.TextTitle,{color:c.PrimeryColor}]}  >متوسط البيع </Text>
                  <NumberFormat
                      value={this.state.BuyUSD}
                      displayType={"text"}
                      thousandSeparator={true}
                       decimalScale={4}
                      fixedDecimalScale={true}
                      renderText={value => (
                        <Text style={[styles.TextNumber,{backgroundColor:c.PrimeryColor,}]} >{value}</Text>
                      )}
                    />
                </TouchableOpacity>

              </View>

            </Animatable.View>
          </View>


          <View style={[styles.TheMainBox,{borderColor:c.PrimeryColor,}]}>
            <Text style={[styles.MainTitle,{color:c.PrimeryColor,borderColor:c.PrimeryColor,}]} > سعر اليورو </Text>
            <Animatable.View animation="slideInDown" iterationCount={1} direction="alternate" >
              <View style={styles.MainBox}>
                <TouchableOpacity style={styles.Boxs}  onPress={this.ListPrice.bind(this,'EU')}>
                  <Text style={[styles.TextTitle,{color:c.PrimeryColor}]} > متوسط الشراء </Text>
                  <NumberFormat
                      value={this.state.SellUo}
                      displayType={"text"}
                      thousandSeparator={true}
                       decimalScale={4}
                      fixedDecimalScale={true}
                      renderText={value => (
                        <Text style={[styles.TextNumber,{backgroundColor:c.PrimeryColor,}]} >{value}</Text>
                      )}
                    />
                 
                </TouchableOpacity>


                <TouchableOpacity style={styles.Boxs}  onPress={this.ListPrice.bind(this,'EU')}>
                  <Text style={[styles.TextTitle,{color:c.PrimeryColor}]}  > متوسط البيع </Text>
                  <NumberFormat
                      value={this.state.BuyUo}
                      displayType={"text"}
                      thousandSeparator={true}
                       decimalScale={4}
                      fixedDecimalScale={true}
                      renderText={value => (
                        <Text style={[styles.TextNumber,{backgroundColor:c.PrimeryColor,}]} >{value}</Text>
                      )}
                    />
                  
                </TouchableOpacity>

              </View>

              

            </Animatable.View>
            
          </View>

        </View>

        <View style={styles.Footer}>

            {/* <View style={{flexDirection:"row",justifyContent:'space-around'}}>
            <TouchableOpacity style={[styles.SmallbuttonContainer,{backgroundColor:'#27ae60'}]} onPress={this.NextScreen}>
            <Text style={styles.textButton} > بيع عملة</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.SmallbuttonContainer,{backgroundColor:'#2980b9'}]} onPress={this.NextScreen}>
            <Text style={styles.textButton} > شراء عملة</Text>
            </TouchableOpacity>

            
        </View> */}

<Text style={[{color:this.state.PrimeryColor,fontSize:16,  textAlign: "center",
    alignSelf: "center",
   }]} >
اخر تحديث   {this.state.update_time}  
       </Text>
            <TouchableOpacity style={[styles.buttonContainer,{backgroundColor:c.PrimeryColor}]} onPress={this.GetDataFromServer}>
            {this.state.SpinnerS ? 
          <Spinner color='red' />
           : 
           <Text style={[styles.textButton]} >تحديث </Text>
        }
          
            </TouchableOpacity>


        </View>

      </ImageBackground>
  
    );
  }
}

const styles = StyleSheet.create({
  TheH1: {
   
    fontSize: 20,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 25,

  },
  TheMainBox: {
    backgroundColor: colors.SecondColor,
    margin:20,
    borderRadius: 20,
    
    borderWidth:1,
  },
  MainBox: {
    flexDirection: 'row',
    width: '100%',
    //  height:'20%',
    justifyContent: "center",

  },

  Boxs: {
    width: '40%',
    // height: '10%',
    justifyContent: "center",
    alignSelf: "center",
    
  },
  MainTitle: {
    backgroundColor:'#fff',
    overflow: "hidden",
    fontSize: 16,
    textAlign: "center",
    alignSelf: "flex-end",
    fontFamily: 'Kan',
    marginBottom:5,
    padding:10,
    marginTop:-20,
    borderRadius: 20,
  
  
  borderWidth:1,
    // borderWidth:2,
  },
  TextTitle: {
    color: c.PrimeryColor,
    fontSize: 14,

    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    marginTop:0,
  },


  TextNumber: {
    color: colors.SecondColor,
    fontSize: 18,
    width:100,
    height:50,
    textAlign: "center",
    alignSelf: "center",
    // fontFamily: 'Kan',
    margin:5,
    // marginBottom:15,
    
    overflow: "hidden",
    borderRadius: 10,
    // borderColor:'#fff',
    paddingTop:10,
    // borderWidth:2,
  },

  imgBackground: {
    width: '100%',
    height: '100%',
flex:1,
  },
  TopCon: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "center",
  },
  ButtomCon: {
    flex: 5,
    justifyContent: "center",
  },
  Footer: {
    flex: 2,
    justifyContent:'flex-end',
    marginBottom:20,
  },
  buttonContainer: {
    backgroundColor: c.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '90%',
    borderRadius: 6,
    marginTop: 10,
    height: 40,

  }, textButton: {
    color: colors.SecondColor,
    fontSize: 12,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
  },

  SmallbuttonContainer:{
    // backgroundColor: c.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '40%',
    borderRadius: 6,
    marginBottom: 10,

    height: 40,
  },
  inputText: {
    color: c.PrimeryColor,
    fontSize: 15,
    width: '70%',
    backgroundColor: '#ffffff0C',
    flexDirection: 'row',
    paddingVertical: 5,
    borderRadius: 3,
    borderColor: c.PrimeryColor,
    borderWidth: 1,
    width: '70%',
    marginTop: 10,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    textAlign: "center",
    alignSelf: "center",

  },
  TButton: {
    marginTop: 30,
    height: 40,
    textAlign: "center",
    alignSelf: "center",
    color: c.PrimeryColor,
  },
  logo: {
    width: '60%',
    marginTop: 150,
    alignSelf: "center",

  }
});
