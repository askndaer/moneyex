import React from 'react';
import { StyleSheet,SegmentedControlIOS,FlatList, Text, Alert,View,AsyncStorage, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container,  Content, Textarea,Form, Item, Input, Label, Right } from 'native-base';
import colors from './Color';
const logo = require("../assets/logo.png");
import SwitchSelector from 'react-native-switch-selector';
import { CheckBox } from 'react-native-elements';
import { Drawer,Picker } from 'native-base';
import Header from './Header';
import MySiderBar from './MySiderBar';
import SideBar from './MySiderBar';
import {  Spinner } from 'native-base';
import HideWithKeyboard from 'react-native-hide-with-keyboard';
import { KeyboardAvoidingView } from 'react-native';
import Config from './Config'
c = new Config
export default class user_register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 0,
      dataSource1:"",
      isLoading: true,
      problem:'',
      SpinnerS:false,
      checked3:false,
      client_id:null,
      title:'',
    };
    this.getKey();
  }
  onValueChange(value) {
    this.setState({
      selected: value
    });
  }
  async getKey() {
    try {
        const id = await AsyncStorage.getItem('@MySuperStore:id');
        this.setState({ client_id: id });
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
}
navigateToScreen = (route) => () => {

  const navigateAction = NavigationActions.navigate({
      routeName: route,
      header: null,
  });
  this.props.navigation.dispatch(navigateAction, {date: new Date()});
  
}
  newComp(id,company_name) {
 
      const { problem } = this.state;
      const { client_id } = this.state;
      const { title } = this.state;
      

      if (title==''){
        alert('يرجاء كتابه عنوان الرسالة')
        return
      }
      if (problem==''){
        alert('يرجاء كتابه تفاصيل الرسالة')
        return
      }
 let Mybody=
 
  
  "message_title="+ title+"&"+
  
  "client_id="+ client_id+"&"+
 
    "detials="+ problem+"";
    
     this.setState({
       SpinnerS: true
     })
    //  alert(Mybody);

    //  'message_title' => $_POST['message_title'],
    //  'types' => $_POST['types'],
    //  'detials' => $_POST['detials'],
    //  'client_id' => $_POST['client_id'],
    //  'company_id' => $_POST['company_id'],
    //  'status' => 1,
    //  'created_at' => current_datetime()
     fetch('http://konuze.com/index.php/api/example/message_add', {
       method: 'POST',
         headers: new Headers({
           'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
         }),
       body: Mybody
        })
     .then((response) => response.json())
     .then((responseJson) => {
       this.setState({
         SpinnerS: false
       })
       console.log("11111111111111111111111111111111111111111");
       console.log(responseJson);
       let MyResponse = responseJson[0];
       if (MyResponse.response==1){
        Alert.alert("تنبية", MyResponse.message);
         this.navigateToScreen('Main');
        
       }else{
         Alert.alert("تنبية", MyResponse.message);
         this.navigateToScreen('Main');
       }
      }).catch((error) => {
        Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
       this.setState({
         SpinnerS: false
       })
     });
   
 }

  render() {

    const { navigation } = this.props;
    const id = navigation.getParam('id', '0');
    const company_name = navigation.getParam('company_name', '0');

    

    return (

    


      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/main.png')}>
<Header  navigation={this.props.navigation} />
  
<HideWithKeyboard>
           <Text style={styles.TheH1}>
انشاء رسالة جديدة
            </Text>
        
            </HideWithKeyboard>
     <View style={{marginLeft:'20%',marginRight:'20%'}}>
      
    
        </View>
        <KeyboardAvoidingView style={{ backgroundColor: '#ffffff00' , flex:1,}} behavior="padding" enabled>
      
          <Content > 
          <Animatable.View ref="view1"  animation="slideInUp" iterationCount={1} direction="alternate" >
            <Form style={styles.Myfrom}>
             
              <Item stackedLabel >
                <Label style={styles.Label1}>عنوان الرسالة  </Label>
                <Input onChangeText={title => this.setState({ title })} style={[styles.MyInput,{textAlign:"center"}]} />
              </Item>
            
               

  <Item stackedLabel last>
                <Label style={styles.Label1}>تفاصيل الرسالة  </Label>
         
                <Textarea  onChangeText={problem => this.setState({ problem })} rowSpan={3} bordered placeholder="اكتب هنا عنوان تفاصيل المشكلة" style={[styles.MyInput,{height:60,}]} />
              </Item>
             



            </Form>
            </Animatable.View>
          </Content>
    

        
    

<View  style={{alignContent:'flex-end',margin:10}}>
<TouchableOpacity style={[styles.buttonContainer,{backgroundColor: c.PrimeryColor,}]} onPress={this.newComp.bind(this,id,company_name)}>
{this.state.SpinnerS ? 
          <Spinner color='red' />
           : 
           <Text style={styles.textButton} > اضافة رسالة جديدة </Text>
        }
          </TouchableOpacity>


</View>
</KeyboardAvoidingView>
      </ImageBackground>
   
    );
  }
}

const styles = StyleSheet.create({

  imgBackground: {
    width: '100%',
    height: '100%',

  },
  TheH1: {
    color: c.PrimeryColor,
    fontSize: 18,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 10,
    
  },
  Label1: {

    color: c.PrimeryColor,
    textAlign: "right",
    fontFamily: 'Kan',
    width: '100%',
    marginTop: 20,
  },
  MyInput: {
    height: 30,
    textAlign: 'right',
    width: '100%',
    borderColor: c.PrimeryColor,
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    color: c.PrimeryColor,
  },
  Myfrom: {
    borderRadius: 20,
    padding: 20,

  },
  buttonContainer: {
    
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 20,
    height: 40,

  }
  , textButton: {
    color: c.SecondColor,
    fontSize: 15,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
});
