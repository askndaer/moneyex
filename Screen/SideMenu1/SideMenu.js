/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

// onPress={this.navigateToScreen('Page1')}
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import styles from './SideMenu.style';
import {NavigationActions} from 'react-navigation';
import {ScrollView, StyleSheet,Text,Image,ImageBackground, View} from 'react-native';

class SideMenu extends Component {
  
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
      header: null,
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render () {
    return (
      
      <View style={styles.container}>
       
       <ImageBackground
//   source={require("../../assets/Wallpaper.png")}
  style={{width: '100%', height: '100%'}}
> 
        <ScrollView>
        <View style={styles1.containerImg}>
      
        <View  > 
        <Text style={{color:'#2a2929',fontSize:20,}} >
              Fares Mohammed
              </Text>
              <Text  style={{color:'#2a2929'}}>
              askndaer@gmail.com
              </Text>

        </View>
        </View>

<View
  style={{
    borderBottomColor: '#77777d',
    borderBottomWidth: 1,
    margin:15,
  }}
/>
        
           



            <View style={{  flexDirection: 'row', }}>
           
              <Text style={styles1.navItemStyle} onPress={this.navigateToScreen('Main')}>
              Main Page
              </Text>
              </View>
            <View style={styles1.link}/>
        
      
            <View style={{  flexDirection: 'row', }}>
          
              <Text style={styles1.navItemStyle} onPress={this.navigateToScreen('ListView')}>
              List View
              </Text>
              </View>
              <View style={styles1.link}/>

             <View style={{  flexDirection: 'row', }}>
          
              <Text style={styles1.navItemStyle} onPress={this.navigateToScreen('login')}>
              login
              </Text>
              </View>
              <View style={styles1.link}/>

   <View style={{  flexDirection: 'row', }}>
           
              <Text style={styles1.navItemStyle} onPress={this.navigateToScreen('Register')}>
              Register
              </Text>
              </View>
              <View style={styles1.link}/>

  <View style={{  flexDirection: 'row', }}>
        
              <Text style={styles1.navItemStyle} onPress={this.navigateToScreen('MyCarousel')}>
              MyCarousel
              </Text>
              </View>
              <View style={styles1.link}/>


  <View style={{  flexDirection: 'row', }}>
        
              <Text style={styles1.navItemStyle} onPress={this.navigateToScreen('MyMap')}>
              MyMap
              </Text>
              </View>
              <View style={styles1.link}/>

  <View style={{  flexDirection: 'row', }}>
        
              <Text style={styles1.navItemStyle} onPress={this.navigateToScreen('FetchEx')}>
              Fetch Example
              </Text>
              </View>
              <View style={styles1.link}/>



        </ScrollView>
        <View style={styles.footerContainer}>
          <Text style={{color:'#fff',}}>LOGOUT</Text>
        </View>
        </ImageBackground>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object,
  
};

export default SideMenu;


const styles1 = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#130f40',
      alignItems: 'center',
      justifyContent: 'center',
    },
    navItemStyle: {
      
        padding: 15,
     
      },
    text:{
      height:40,margin:10,
      alignItems:'center' ,
 
      padding: 10,
      borderRadius: 6,
      
      
    },
    containerImg:{
        flexDirection: 'row', 
        marginLeft:10,
        width: '100%',
        alignItems:'center' ,
        marginTop:30,
    },
    logo: {
     
    margin:10,
      height: 80,
      width: 80,
      // resizeMode: Image.resizeMode.contain,
    },

    NodeIMG: {
     
          margin:10,
          marginLeft:20,
          height: 35,
          width: 35,
          // resizeMode: Image.resizeMode.contain,
        },
        link:{

            borderBottomColor: '#c8bfbf',
            borderBottomWidth: 1,
            margin:15,
        }
  });
  

