import React from 'react';
import { StyleSheet,AsyncStorage,SegmentedControlIOS,KeyboardAvoidingView,Alert, Text, View, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container, Header, Content, Textarea,Form, Item, Input, Label, Right } from 'native-base';
import colors from './Color';
import { NavigationActions } from 'react-navigation';
const logo = require("../assets/logo.png");
import SwitchSelector from 'react-native-switch-selector';
import Header1 from './Header';
import { CheckBox } from 'react-native-elements';
import HideWithKeyboard from 'react-native-hide-with-keyboard';
import {  Spinner } from 'native-base';
import { Ionicons ,FontAwesome,Entypo} from '@expo/vector-icons';


export default class user_register extends React.Component {

  constructor (props) {
    super(props);
   
   
}




  render() {


    return (

      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/main.png')}>
<Header1  navigation={this.props.navigation}  />



           <Text style={styles.TheH1}>
            انشاء حساب حديد
            </Text>
        
    
<View>
<TouchableOpacity 
onPress={()=>{this.props.navigation.push("RegisterUser");}}
style={{margin:20,width:'80%',height:150,justifyContent:'center',alignItems:'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:20,borderColor:colors.PrimeryColor,borderWidth:1}}>

<FontAwesome name="building-o" size={32} color={colors.PrimeryColor} />
<Text style={styles.TheH1}>
            انشاء حساب لمستخدم
            </Text>
</TouchableOpacity>

<TouchableOpacity
onPress={()=>{this.props.navigation.push("RegisterCompany");}}
 style={{margin:20,width:'80%',height:150,justifyContent:'center',alignItems:'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:20,borderColor:colors.PrimeryColor,borderWidth:1}}>

<Entypo name="users" size={32} color={colors.PrimeryColor} />
<Text style={styles.TheH1}>
            انشاء حساب لشركة
            </Text>
</TouchableOpacity>


</View>



      </ImageBackground>
      
    );
  }
}

const styles = StyleSheet.create({

  imgBackground: {
    width: '100%',
    height: '100%',
flex:1
  },
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 18,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 10,
    
   
  },
  Label1: {

    color: colors.PrimeryColor,
    textAlign: "right",
    fontFamily: 'Kan',
    width: '100%',
    marginTop: 20,
  },
  MyInput: {
    height: 30,
    textAlign: 'right',
    width: '100%',
    borderColor: colors.PrimeryColor,
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    color: colors.PrimeryColor,
  },
  Myfrom: {
    borderRadius: 20,
    padding: 20,

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 10,
    height: 40,
    marginBottom:20

  }
  , textButton: {
    color: colors.SecondColor,
    fontSize: 12,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
});
