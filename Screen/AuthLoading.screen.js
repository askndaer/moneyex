import React, { Component } from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    Button,
    StatusBar,
    StyleSheet,
    View,
  } from 'react-native';
export default class AuthLoadingScreen extends React.Component {
    constructor() {
      super();
      console.disableYellowBox = true;
      this._bootstrapAsync();
    }
  
    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {
      const userToken = await AsyncStorage.getItem('userToken');
      // const appFirstTimeToken = await AsyncStorage.getItem('appFirstTime');
      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      // if(appFirstTimeToken){
        console.log("ccccccccccccccccccccc     cvvvvvvvvvvvvvvvv  ");
        console.log("ccccccccccccccccccccc     cvvvvvvvvvvvvvvvv  ");
        console.log("ccccccccccccccccccccc     cvvvvvvvvvvvvvvvv  ");
        
        const Type = await AsyncStorage.getItem('@MySuperStore:Type');
        console.log("ccccccccccccccccccccc     cvvvvvvvvvvvvvvvv  "+Type)
        if (Type=='company')
        this.props.navigation.navigate(userToken ? 'LoginNCompany' : 'LogOutN');
        else
        this.props.navigation.navigate(userToken ? 'LoginN' : 'LogOutN');
      
      // }else{
        // this.props.navigation.navigate('AppFirstTime');
      // }
      
    };
  
    // Render any loading content that you like here
    render() {
      return (
        <View style={styles.container}>
          <ActivityIndicator />
          <StatusBar barStyle="default" />
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });