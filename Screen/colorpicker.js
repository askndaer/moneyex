import React from 'react';
import { StyleSheet,AsyncStorage,BackHandler, Text, View,Alert,Dimensions,KeyboardAvoidingView, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Drawer,Thumbnail } from 'native-base';
import MySiderBar from './MySiderBar';
import colors from './Color';
import Header from './Header';
import { DrawerNavigator } from "react-navigation";
import {  Spinner } from 'native-base';
import Moment from 'moment';

import { ColorWheel } from 'react-native-color-wheel';
import Config from './Config'

c = new Config


export default class login extends React.Component {
 
  constructor() {
    super();
   
    this.state = {
      PrimeryColor: global.PrimeryColor,
      }

}
  
  
  saveKeyEmail_Name_phone = async (PrimeryColor) => {
   
         AsyncStorage.setItem('@MySuperStore:PrimeryColor', PrimeryColor);
      
        }
  
       
  
   
  
    navigateToScreen(route,x) {
      const navigateAction = NavigationActions.navigate({
        routeName: route,
        header: null,
      });
      this.props.navigation.navigate(x,navigateAction);
    }
    
    
    onChange(color){
      c.getTranslations1(color);
      this.saveKeyEmail_Name_phone(color);
      this.setState({ PrimeryColor: color });
      this.props.navigation.navigate('Login')
    }
  
  
  
    render() {
  
      return (
        <View style={{flex:1}}>
  <KeyboardAvoidingView style={styles.container} behavior="padding"  > 
  
        <ImageBackground style={styles.imgBackground}
          resizeMode='cover'
          source={require('../assets/main.png')}>

                    
<Text style={styles.TheH1}>
          لون البرنامج الحالي هو 
            </Text>
            <TouchableOpacity style={{margin:'5%',width:'90%',height:60,borderRadius:60,backgroundColor:this.state.PrimeryColor,}} >
        
        </TouchableOpacity>

            
<Text style={styles.TheH1}>
           يرجاء اختيار لون البرنامج
            </Text>
<View style={{flex:1,justifyContent:'flex-start',margin:20,marginTop:0, flexDirection:'row',flexWrap:'wrap',justifyContent:'center'}}>

<TouchableOpacity style={{margin:15,width:60,height:60,borderRadius:60,backgroundColor:'#16a085',}}  onPress={this.onChange.bind(this,'#16a085')}>
        
          </TouchableOpacity>
          <TouchableOpacity style={{margin:15,width:60,height:60,borderRadius:60,backgroundColor:'#3d6bb4',}} onPress={this.onChange.bind(this,'#2980b9')}>
        
        </TouchableOpacity>

        <TouchableOpacity style={{margin:15,width:60,height:60,borderRadius:60,backgroundColor:'#8e44ad',}} onPress={this.onChange.bind(this,'#8e44ad')}>
        
        </TouchableOpacity>
        <TouchableOpacity style={{margin:15,width:60,height:60,borderRadius:60,backgroundColor:'#2c3e50',}} onPress={this.onChange.bind(this,'#2c3e50')}>
      
      </TouchableOpacity>
      
<TouchableOpacity style={{margin:15,width:60,height:60,borderRadius:60,backgroundColor:'#f39c12',}} onPress={this.onChange.bind(this,'#f39c12')}>
        
        </TouchableOpacity>
        <TouchableOpacity style={{margin:15,width:60,height:60,borderRadius:60,backgroundColor:'#d35400',}}  onPress={this.onChange.bind(this,'#d35400')}>
      
      </TouchableOpacity>
      
<TouchableOpacity style={{margin:15,width:60,height:60,borderRadius:60,backgroundColor:'#c0392b',}}  onPress={this.onChange.bind(this,'#c0392b')}>
        
        </TouchableOpacity>
        <TouchableOpacity style={{margin:15,width:60,height:60,borderRadius:60,backgroundColor:'#7f8c8d',}}  onPress={this.onChange.bind(this,'#7f8c8d')}>
      
      </TouchableOpacity>
      
<TouchableOpacity style={{margin:15,width:60,height:60,borderRadius:60,backgroundColor:'#F97F51',}}  onPress={this.onChange.bind(this,'#F97F51')}>
        
        </TouchableOpacity>
        <TouchableOpacity style={{margin:15,width:60,height:60,borderRadius:60,backgroundColor:'#40407a',}}  onPress={this.onChange.bind(this,'#40407a')}>
      
      </TouchableOpacity>

    </View>


    
  
        </ImageBackground>
            </KeyboardAvoidingView>
            </View>
      );
    }
    
  }
  
  const styles = StyleSheet.create({
    container: {
  
      flex: 1,
  
      alignItems: 'center',
      justifyContent: 'center',
    },
  TheH1: {
    color: colors.PrimeryColor,
    fontSize: 18,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
    margin: 35,
   
  },
    imgBackground: {
      width: '100%',
      height: '100%',
      flex: 1,
    },
    TopCon: {
      flex: 1,
  
    },
    ButtomCon: {
      flexDirection: 'column',
      flex: 2,
      justifyContent: 'flex-end'
  
    },
    buttonContainer: {
      backgroundColor: colors.PrimeryColor,
      justifyContent: "center",
      alignSelf: "center",
      width: '70%',
      borderRadius: 6,
      marginTop: 20,
      height: 40,
  
    }, textButton: {
      color: colors.SecondColor,
      fontSize: 15,
      fontFamily: 'Kan',
      textAlign: "center",
      alignSelf: "center",
    },
    inputText: {
      fontFamily: 'Kan',
      color: colors.PrimeryColor,
      fontSize: 15,
      width: '70%',
      backgroundColor: '#ffffff0C',
      flexDirection: 'row',
      paddingVertical: 5,
      borderRadius: 3,
      borderColor: colors.PrimeryColor,
      borderWidth: 1,
      width: '70%',
      marginTop: 10,
      borderTopWidth: 0,
      borderRightWidth: 0,
      borderLeftWidth: 0,
      textAlign: "center",
      alignSelf: "center",
  
    },
    TButton: {
      fontFamily: 'Kan',
      marginTop: 30,
      height: 40,
      textAlign: "center",
      alignSelf: "center",
      color: colors.PrimeryColor,
    },
  
    logo: {
      width: '80%',
      // backgroundColor:'#000',
      height:100,
      marginTop: 150,
      alignSelf: "center",
    }
  });
  