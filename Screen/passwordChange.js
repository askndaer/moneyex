import React from 'react';
import { StyleSheet,SegmentedControlIOS,Alert, Text, View,AsyncStorage, ImageBackground, TouchableOpacity, Image, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container,  Content, Textarea,Form, Item, Input, Label, Right } from 'native-base';
import colors from './Color';
const logo = require("../assets/logo.png");
import SwitchSelector from 'react-native-switch-selector';
import { Divider } from 'react-native-elements';
import { Drawer,Thumbnail } from 'native-base';
import Header from './Header';
import MySiderBar from './MySiderBar';
import SideBar from './MySiderBar';
import {  Spinner } from 'native-base';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Config from './Config'
import HideWithKeyboard from 'react-native-hide-with-keyboard';

c = new Config



export default class user_register extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      id: '',
      newpassword:'',
      newpassword2:'',
      oldpassword:'',
      SpinnerS:false
    };
    this.getKey() ;
}

  async getKey() {
    try {
        const L_id = await AsyncStorage.getItem('@MySuperStore:id');
        this.setState({ id: L_id });
    } catch (error) {
        console.log("Error retrieving data" + error);
    }
}

Signup = async () => {
  console.log("Error retrieving datasssssssssssss"+this.state.newpassword +"    "+this.state.newpassword2  );

  const { newpassword } = this.state;
  const { newpassword2 } = this.state;
  const { oldpassword } = this.state;
  const { id } = this.state;

  if (newpassword  == newpassword2){  
    console.log("Error2222222222222222222222222222222222222"  );
  let Mybody="newpassword="+ newpassword+"&"+
              "oldpassword="+ oldpassword+"&"+
              "id="+ id+"";
     

  this.setState({
    SpinnerS: true
  })
  fetch('http://konuze.com/index.php/api/example/UpdatePasswordClient', {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
      }),
      body: Mybody
    }).then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        SpinnerS: false
      })
      let MyResponse = responseJson[0];
      if (MyResponse.response==1){
        Alert.alert("تنبية", MyResponse.message);
      }else{
        Alert.alert("تنبية", MyResponse.message);
      }
     }).catch((error) => {
      Alert.alert("تنبية", "لا يمكن الاتصال بسرفر");
      this.setState({
        SpinnerS: false
      })
    });
  }else{
    // console.log("333333333333333333333333"  );
    Alert.alert("تنبية", 'كلمة السر غير متتابطة');
  }
}


  render() {
    return (
      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../assets/main.png')}>
        
<Header  navigation={this.props.navigation}  />
<HideWithKeyboard>
<View style={{margin:5}}>
           <Text style={[styles.TheH1,{color:c.PrimeryColor}]}>
           تغيير كلمة السر
            </Text>
            </View>  
        </HideWithKeyboard>
     <View style={{marginLeft:'20%',marginRight:'20%'}}>
      
    
        </View>
        <Container style={{ backgroundColor: '#ffffff00', margin: 10 , flex:1,}}>
          <Content > 
          <Animatable.View ref="view1"  animation="slideInUp" iterationCount={1} direction="alternate" >
            <Form style={styles.Myfrom}>
              <Item stackedLabel >
                <Label style={[styles.Label1,{color:c.PrimeryColor}]}>كلمة المرورالحالية </Label>
                <TextInput   blurOnSubmit={false}   returnKeyType = {"next"} onSubmitEditing={() => { this.secondTextInput.focus(); }}    onChangeText={oldpassword => this.setState({ oldpassword })} style={[styles.MyInput,{ borderColor: c.PrimeryColor,color:c.PrimeryColor}]} />
              
              </Item>
              
                 <Item stackedLabel last>
                <Label style={[styles.Label1,{color:c.PrimeryColor}]}> كلمة الجديدة  </Label>
                <TextInput  blurOnSubmit={false}  onSubmitEditing={() => { this.thirdTextInput.focus(); }}  returnKeyType = {"next"}  ref={(input) => { this.secondTextInput = input; }}  secureTextEntry={true}   onChangeText={newpassword => this.setState({ newpassword })} style={[styles.MyInput,{ borderColor: c.PrimeryColor,color:c.PrimeryColor}]} />
            
              </Item>
                 <Item stackedLabel last>
                <Label style={[styles.Label1,{color:c.PrimeryColor}]}> تاكيد كلمة السر الجديدة  </Label>
                <TextInput   ref={(input) => { this.thirdTextInput = input; }}  returnKeyType = { "next" } secureTextEntry={true}   onChangeText={newpassword2 => this.setState({ newpassword2 })} style={[styles.MyInput,{ borderColor: c.PrimeryColor,color:c.PrimeryColor}]} />
             
              </Item>

             
            </Form>
            </Animatable.View>
          </Content>
    


        </Container>

<View  style={{alignContent:'flex-end',marginBottom:10}}>
<TouchableOpacity style={[styles.buttonContainer,{backgroundColor:c.PrimeryColor}]} onPress={()=> this.Signup()}>
{this.state.SpinnerS ? 
  <Spinner color='red' />
   : 
   <Text style={styles.textButton} > حفظ </Text>
}
          
          </TouchableOpacity>
          

</View>
<KeyboardSpacer/>
      </ImageBackground>
     
    );
  }
}

const styles = StyleSheet.create({

  imgBackground: {
    width: '100%',
    height: '100%',
flex:1
  },
  TheH1: {
  
    fontSize: 18*c.scale,
    textAlign: "center",
    alignSelf: "center",
    fontFamily: 'Kan',
  
  },
  Label1: {
    fontSize: 14*c.scale,
   
    textAlign: "right",
    fontFamily: 'Kan',
    width: '100%',
    marginTop: 10,
  },
  MyInput: {
    fontSize: 14*c.scale,
    height: 30*c.scale,
    textAlign: 'right',
    width: '100%',
   
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
   
  },
  Myfrom: {
    borderRadius: 20,
    padding: 20,

  },
  buttonContainer: {
    backgroundColor: colors.PrimeryColor,
    justifyContent: "center",
    alignSelf: "center",
    width: '70%',
    borderRadius: 6,
    marginTop: 20,
    height: 40,

  }
  , textButton: {
    color: colors.SecondColor,
    fontSize: 15*c.scale,
    fontFamily: 'Kan',
    textAlign: "center",
    alignSelf: "center",
  },
});
